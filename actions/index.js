import {
    SIGNUP_REQUEST,
    SIGNUP_USER_STARTED,
    SIGNUP_USER_SUCCESS,    
    SIGNUP_USER_FAILURE,
    
    LOGIN_REQUEST,
    LOGIN_STARTED,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,

   GET_HISTORY_REQUEST,
   GET_HISTORY_STARTED,
   GET_HISTORY_SUCCESS,
   GET_HISTORY_FAILURE,

    GET_USER_REWARD_POINT_REQUEST,
    GET_USER_REWARD_POINT_STARTED,
    GET_USER_REWARD_POINT_SUCCESS,
    GET_USER_REWARD_POINT_FAILURE,

    GET_CATEGORY_REQUEST,
    GET_CATEGORY_STARTED,
    GET_CATEGORY_SUCCESS,
    GET_CATEGORY_FAILURE,

    GET_SUBCATEGORY_REQUEST,
    GET_SUBCATEGORY_STARTED,
    GET_SUBCATEGORY_SUCCESS,
    GET_SUBCATEGORY_FAILURE,

    GET_SEARCHBUSINESS_REQUEST,
    GET_SEARCHBUSINESS_STARTED,
    GET_SEARCHBUSINESS_SUCCESS,
    GET_SEARCHBUSINESS_FAILURE
} from '../types';
import { createAction } from '../util/reduxUtil'


export const userSignUp = (requestBody) =>
    createAction(SIGNUP_REQUEST,{requestBody});
export const signUpStarted = () =>
    createAction(SIGNUP_USER_STARTED);
export const signUpSuccess = (response) =>
    createAction(SIGNUP_USER_SUCCESS,{response});
export const signUpFailure = (response) =>
    createAction(SIGNUP_USER_FAILURE,{response});

export const userLogin = (requestBody) =>
    createAction(LOGIN_REQUEST,{requestBody});
export const loginStarted = () =>
    createAction(LOGIN_STARTED);
export const loginSuccess = (response) =>
    createAction(LOGIN_SUCCESS,{response});
export const loginFailure = (response) =>
    createAction(LOGIN_FAILURE,{response});


export const getHistoryRequest = (requestBody) =>
    createAction(GET_HISTORY_REQUEST,{requestBody});
export const getHistoryStarted = () =>
    createAction(GET_HISTORY_STARTED);
export const getHistorySuccess = (response) =>
    createAction(GET_HISTORY_SUCCESS,{response});
export const getHistoryFailure = (response) =>
    createAction(GET_HISTORY_FAILURE,{response});   

export const getUserRewardsPointsRequest = (requestBody) =>
    createAction(GET_USER_REWARD_POINT_REQUEST,{requestBody});
export const getUserRewardsPointsStarted = () =>
    createAction(GET_USER_REWARD_POINT_STARTED);
export const getUserRewardsPointsSuccess = (response) =>
    createAction(GET_USER_REWARD_POINT_SUCCESS,{response});
export const getUserRewardsPointsFailure = (response) =>
    createAction(GET_USER_REWARD_POINT_FAILURE,{response});   

export const getCategoryRequest = (requestBody) =>
    createAction(GET_CATEGORY_REQUEST,{requestBody});
export const getCategoryStarted = () =>
    createAction(GET_CATEGORY_STARTED);
export const getCategorySuccess = (response) =>
    createAction(GET_CATEGORY_SUCCESS,{response});
export const getCategoryFailure = (response) =>
    createAction(GET_CATEGORY_FAILURE,{response});   

export const getSubCategoryRequest = (requestBody) =>
    createAction(GET_SUBCATEGORY_REQUEST,{requestBody});
export const getSubCategoryStarted = () =>
    createAction(GET_SUBCATEGORY_STARTED);
export const getSubCategorySuccess = (response) =>
    createAction(GET_SUBCATEGORY_SUCCESS,{response});
export const getsubCategoryFailure = (response) =>
    createAction(GET_SUBCATEGORY_FAILURE,{response});   

export const getSearchBusinessRequest = (requestBody) =>
    createAction(GET_SEARCHBUSINESS_REQUEST,{requestBody});
export const getSearchBusinessStarted = () =>
    createAction(GET_SEARCHBUSINESS_STARTED);
export const getSearchBusinessSuccess = (response) =>
    createAction(GET_SEARCHBUSINESS_SUCCESS,{response});
export const getSearchBusinessFailure = (response) =>
    createAction(GET_SEARCHBUSINESS_FAILURE,{response});   
    

