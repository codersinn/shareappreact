import React from 'react';
import { Platform,StyleSheet, Image, ImageBackground, BackHandler, FlatList, Text, View, TouchableHighlight, TouchableWithoutFeedback, SafeAreaView,Dimensions } from 'react-native';
import Amplify from 'aws-amplify';
import { Auth } from 'aws-amplify';
import awsConfig from './Src/aws-exports';
import { Col } from 'native-base';
import Tabbar from 'react-native-tabbar-bottom'
//var SharedPreferences = require('react-native-shared-preferences');.
import AsyncStorage from '@react-native-community/async-storage';
const DEVICE_HEIGHT = Dimensions.get('window').height - ( Platform.OS === 'ios' ? 180 : 120);
import { MaterialIndicator } from 'react-native-indicators';
import SideMenu from 'react-native-side-menu';
import Menu from './Menu';
Amplify.configure(awsConfig);

Auth.currentAuthenticatedUser({
  bypassCache: true  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
}).then(user => {

  // this.storeItem("username",JSON.stringify(user.username))
  // this.storeItem("email",JSON.stringify(user.signInUserSession.idToken.payload.email))
  // this.storeItem("key","value")
  // this.storeItem("event_id",JSON.stringify(user.signInUserSession.idToken.payload.event_id))
  // this.storeItem("phone_number",JSON.stringify(user.signInUserSession.idToken.payload.phone_number))
  // this.storeItem("client_id",JSON.stringify(user.signInUserSession.accessToken.payload.client_id)) 

  email = user.signInUserSession.idToken.payload.email

}

)
  .catch(err => {

    // SharedPreferences.setItem("username",JSON.stringify(user.username))
    // SharedPreferences.setItem("email",JSON.stringify(user.signInUserSession.idToken.payload.email))
    // SharedPreferences.setItem("key","value")
    // SharedPreferences.setItem("event_id",JSON.stringify(user.signInUserSession.idToken.payload.event_id))
    // SharedPreferences.setItem("phone_number",JSON.stringify(user.signInUserSession.idToken.payload.phone_number))
    // SharedPreferences.setItem("client_id",JSON.stringify(user.signInUserSession.accessToken.payload.client_id))


  })

import { withAuthenticator, PhoneField } from 'aws-amplify-react-native';


import { getHistoryRequest, getUserRewardsPointsRequest, getCategoryRequest,getSubCategoryRequest } from './actions'
import { connect } from 'react-redux';
import ProfileScreen from './Component/ProfileScreen'
var email = 'include@gmail.com';
var phone = '123456789';
var username = "username"
var data = []
const arrayEarnRewards = [

  {
    name: 'Reviews',
    img: require('./img/reviewscategory.jpg')
  },
  {
    name: 'like,\ncomment,\nshare',
    img: require('./img/socialmediacategory.jpg')
  },
  {
    name: 'IG Post',
    img: require('./img/instagramselfie.jpg')
  },
  {
    name: 'Referrals',
    img: require('./img/referralscategory.jpg')
  },
  {
    name: 'Check In',
    img: require('./img/fbcheckin.jpg')
  },
  {
    name: 'memorabilla',
    img: require('./img/memorabilla.jpg')
  },

]
const arrayShop = [

  {
    name: '1 Free\nSession',
    img: require('./img/freetshirt.jpg'),
    points: '750 Points'
  },
  {
    name: '10% off\n1 month',
    img: require('./img/yogamat.jpg'),
    points: '200 Reward Points'

  },
  {
    name: '10% off\n3 month',
    img: require('./img/tendiscount.jpg'),
    points: '500 Reward Points'

  },
  {
    name: 'branded\nclothing',
    img: require('./img/freepersonalt.jpg'),
    points: '900 Points'

  },
  {
    name: 'Meal Plan',
    img: require('./img/freemealplan.jpg'),
    points: '500 Points'

  },
  {
    name: 'unlimited for\n30 days',
    img: require('./img/freemealplan.jpg'),
    points: '1250 Points'

  },

]
class AppScreen extends React.Component {

  // static navigationOptions = {
  //   header: null,
  // };



  constructor() {
    super()
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.getdataFromSharedPreference();
    this.state = {
      animating: false,
      data: [],
      token: '',
      pageType: "HomeScreen",
      title : 'Shop',
    }
  }



  componentWillMount() {
    
   
  

      const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      
      const item = this.props.navigation.getParam('item', 'NO-ID')
    
      const itpem=this.props.navigation.getParam('pageType', 'NO-ID')

      
        this.setState({pageType:'HomeScreen'})
        const tt  = this.props.navigation.getParam('head','NO-ID')
        this.setState({title:item.name})
      
  // this.props.getSubCategoryRequest({
  //     category_id: item.id
  //       })
    });
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }



  getdataFromSharedPreference() {

    try {

      AsyncStorage.getItem('email', (err, result) => {
        email = result;

      });
      AsyncStorage.getItem('phone_number', (err, result) => {
        phone = result;
        console.log(phone);
      });
      AsyncStorage.getItem('username', (err, result) => {
        username = result;
        console.log(username);
      });

    }
    catch (e) {
      console.error(e.message);
    }

  }


  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    //alert('hello');
    BackHandler.exitApp();
    //this.props.navigation.goBack(null);
    return true;
  }

  sendtoproduct(title, imageuri, price, coin) {
    this.props.navigation.navigate('ProductPurchase',
      {
        tit: title,
        imageurl: imageuri,
        pric: price,
        coi: coin
      })
  }




  _renderItem(item) {
    return (
      <Col style={{ padding: 1, paddingLeft: 0 }}>

        <ImageBackground
          source={item.img}

          style={{ flex: 1, resizeMode: 'cover', height: ((DEVICE_HEIGHT) / 3) }}>
          <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.5)', width: '100%', flex: 1 }}>

            <TouchableHighlight onPress={() => {
              this.props.navigation.navigate('Details', { item: item })
            }}>
              <Text style={{ color: 'white', fontSize: 20 }}>{item.name}</Text>
            </TouchableHighlight>
          </View>

        </ImageBackground>

      </Col>

    )
  }

  _renderSubCategoryItem(item) {
    return (
      <TouchableHighlight
      onPress={()=>{
         this.setState({title:item.name})
       
        this.setState({pageType:"HomeScreen"})
      }}
      >
      <View style={{ flexDirection: 'row',padding:10,alignItems:'center' }}>

        <Image
          style={{ width: 100, height: 70 }}
          source={{ uri: item.image }}
        />

        <Text style={{fontSize:25,fontFamily:'Utsaah',marginLeft:20}}>{item.name}</Text>
      </View>
      </TouchableHighlight>
    )
  }

  _categoryRenderItem(item) {

    return (
      <Col style={{ padding: 1, paddingLeft: 0 }}>

        <ImageBackground
          source={{ uri: item.image }}
          style={{ flex: 1, resizeMode: 'cover', height: (DEVICE_HEIGHT / 2.75) }}>
          <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.5)', width: '100%', flex: 1 }}>

            <TouchableHighlight onPress={() => {
              // this.props.navigation.navigate('Details',{item:item})
              this.setState({ pageType: 'SubCategory' })
              this.props.getSubCategoryRequest({
                category_id: item.id
                  })

            }}>
              <Text style={{ color: 'white', fontSize: 20, fontFamily: 'Hobo Std' }}>{item.name}</Text>
            </TouchableHighlight>
          </View>

        </ImageBackground>

      </Col>

    )
  }

  _renderItemProfileScreen(item) {
    return (
      <Col style={{ padding: 1, paddingLeft: 0 }}>
        <ImageBackground source={item.img} style={{ flex: 1, resizeMode: 'cover', height: (DEVICE_HEIGHT / 3) }}>
          <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.5)', width: '100%', flex: 1 }}>
            <Text style={{ color: 'white', fontSize: 24, textAlign: 'center' }}>{item.name}</Text>

            <Text style={{ color: 'white' }}> {item.points}  </Text>

            <TouchableWithoutFeedback onPress={() => this.sendtoproduct('1 Free Session', '0', 'a viva pant', 750)}>
              <Text style={{ backgroundColor: '#5372C4', color: 'white', margin: 10, padding: 10 }}> Shop Now </Text>
            </TouchableWithoutFeedback>
          </View>
        </ImageBackground>
      </Col>
    )
  }

  _renderItemNotification = (item) => {
    return (
      <View style={{ flex: 1, flexDirection: 'row', height: 52, width: '100%', position: 'relative', marginLeft: 5, marginRight: 5 }}>
        <View style={{ flot: 'left', flexDirection: 'column', marginTop: 5 }}>
          <Text>Reward Point by {item.type}</Text>
          <Text>{item.created_at}</Text>
        </View>
        <Text style={{ backgroundColor: '#5372C4', color: '#fff', padding: 5, right: 10, marginTop: 5, position: 'absolute', fontWeight: 'bold', fontSize: 15 }}>{item.points} Points</Text>
      </View>)
  }

  _renderItemChatScreen(item) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', height: 72, width: '100%', position: 'relative', marginLeft: 5, marginRight: 5 }}>
        <View style={{ flot: 'left', flexDirection: 'column', marginTop: 5 }}>
          <Text>Order Id : {item.order_id}</Text>
          <Text>{item.product_name}</Text>
          <Text>{item.created_at}</Text>
        </View>
        <Text style={{ backgroundColor: '#5372C4', color: '#fff', padding: 5, right: 10, marginTop: 5, position: 'absolute', fontWeight: 'bold', fontSize: 15 }}>{item.product_point} Points</Text>
      </View>)
  }

  _headerSubCategoryBar = () => {
    const item=this.props.navigation.getParam('item', 'NO-ID')

    return (
      <View style={styles.headerView}>
        <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack()}>
          <View style={{ width: 50, height: 60, backgroundColor: 'white', zIndex: 1,marginLeft:10 }}>
            <Image
              style={{ width: 30, height: 30, marginTop: 15,rota:180 }}
              
              source={require('./img/back_new.png') }
            />
          </View>
        </TouchableWithoutFeedback>
        
       <Text>{item.name}</Text>
      </View>)
  }
  _headerBar = () => {
    return (
      <View style={styles.headerView}>
        <TouchableWithoutFeedback onPress={() => this.toggleDrawer()}>
          <View style={{ width: 50, height: 60, backgroundColor: 'white', zIndex: 1 }}>
            <Image
              style={{ width: 30, height: 30, marginTop: 15 }}
              source={{ uri: 'https://cdn4.iconfinder.com/data/icons/yellow-commerce/100/.svg-19-512.png' }}
            />
          </View>
        </TouchableWithoutFeedback>
        {this.state.pageType =='Category' && this.getCategoryHeader()}
        {(this.state.pageType !=='Category') && this.getNormalHeader()}
      </View>)
  }

  toggleDrawer() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }


  getNormalHeader(){
    return(
      <Text style={{ color: 'black',  fontSize: 20,marginLeft:-50, fontWeight: 'bold', textAlign: 'center', width: '100%' }}>{this.state.title}</Text>

    )
  }

  getCategoryHeader(){
    return(
      <View style={{
        borderBottomLeftRadius: 20,
        flexDirection: 'row',
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        height: 40,
        flex:1,
       marginRight:20,
       paddingTop:10,
       paddingLeft:10,
        backgroundColor: '#4c4c4c4c', width: '70%'
      }}>
        <Text style={{flex:0.9}}>Enter business name</Text>
        <Image
          style={{ width: 20, height: 20, }}
          source={require('./img/search_new.png')}

        />
      </View>
     
    )
  }

  getItemByType(type, item) {

    var view = {
      'HomeScreen': type == 'HomeScreen' && this._renderItem(item.item),
      'ProfileScreen': type == 'ProfileScreen' && this._renderItemProfileScreen(item.item),
      'NotificationScreen': type == 'NotificationScreen' && this._renderItemNotification(item.item),
      'ChatScreen': type == 'ChatScreen' && this._renderItemChatScreen(item.item),
      'Category': type == 'Category' && this._categoryRenderItem(item.item),
      'SubCategory': type == 'SubCategory' && this._renderSubCategoryItem(item.item),
      'default': 'Default item'
    };
    return (view[type] || view['default']);
  }

  getArrayByType(type) {
    var array = {
      'HomeScreen': arrayEarnRewards,
      'ProfileScreen': arrayShop,
      'NotificationScreen': this.props.rewardsPointsData,
      'ChatScreen': this.props.getRewardsData,
      'Category': this.props.categorydata,
      'SubCategory': this.props.subCategorydata,
      'default': []
    };
    return (array[type] || array['default']);
  }

  getColumnType(type) {
    var columnType = {
      'HomeScreen': 2,
      'ProfileScreen': 2,
      'Category': 2,
      'SubCategory': 1,
      'NotificationScreen': 1,
      'default': 1
    };
    return (columnType[type] || columnType['default']);
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  onMenuItemSelected = item =>
    this.setState({
      isOpen: false,
      selectedItem: item,
    });

  render() {
    const columntype = this.getColumnType(this.state.pageType);

    const menu = <Menu onItemSelected={this.onMenuItemSelected} type='home'      navigation={this.props.navigation}
    />;

    return (
      <SideMenu
     
        menu={menu}
        isOpen={this.state.isOpen}
        onChange={isOpen => this.updateMenuState(isOpen)}
      >
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>

        <View style={styles.container}>

{this.state.pageType =='SubCategory' ? this._headerSubCategoryBar() : this._headerBar()}
          {this.props.loading && <MaterialIndicator color="#5B5A5F" size={30} trackWidth={2} />}
          {!this.props.loading && (this.state.pageType === "Category" ||this.state.pageType === "SubCategory" || this.state.pageType === "HomeScreen" || this.state.pageType === 'ProfileScreen' || this.state.pageType === "NotificationScreen" || this.state.pageType === 'ChatScreen') &&
            <FlatList
              style={{ flex: 1 }}

              renderItem={(item, index) => this.getItemByType(this.state.pageType, item)}
              key={this.state.pageType}
              data={this.getArrayByType(this.state.pageType)}
              numColumns={columntype}
              bounces={false}
            />}



          {this.state.pageType === "SearchScreen" &&

            <ProfileScreen
              email={email}
              phone={phone}
              username={username}
              navigation={this.props.navigation}
            />

          }

          {this.state.pageType !== 'SubCategory' &&
            <Tabbar
              tabbarBgColor="#fff"
              selectedIconColor="#5372C4"
              stateFunc={(tab) => {
                this.setState({ pageType: tab.page })
                console.log(tab.pageType);
                if (tab.page == 'NotificationScreen') {
                  // alert(tab.page);
                  this.props.getUserRewardsPointsRequest({
                    email: null
                  });
                  //  this.call()
                }
                else if (tab.page == 'ChatScreen') {

                  let requestBody = {
                    email: 'include@gmail.com'
                  }
                  //
                  this.props.getHistoryRequest(requestBody);
                }

                //this.props.navigation.setParams({tabTitle: tab.title})
              }}
              activePage={this.state.pageType}

              tabs={[
                {
                  page: "HomeScreen",
                  icon: "home",
                },
                {
                  page: "NotificationScreen",
                  icon: "paper"
                },
                {
                  page: "ProfileScreen",
                  icon: "cart",
                },
                {
                  page: "ChatScreen",
                  icon: "reorder",
                },
                {
                  page: "SearchScreen",
                  icon: "settings",
                },
              ]}
            />
          }
        </View>
      </SafeAreaView>
    </SideMenu>
    );
  }
}


const mapStateToProps = (state) => {  
  return {
    loading: state.loading,
    getRewardsData: state.getRewardsData,
    rewardsPointsData: state.rewardsPointsData,
    categorydata: state.categoryArray,
    subCategorydata: state.subCategoryArray,
  }
};

const mapDispatchToProps = {
  getHistoryRequest,
  getUserRewardsPointsRequest,
  getCategoryRequest,
  getSubCategoryRequest
};

const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppScreen);

export default App;
//export default withAuthenticator(App);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  headerView: {
    backgroundColor: 'white', alignItems: 'center', width: '100%', flexDirection: 'row', height: 60,
    justifyContent: 'flex-start',
  }
});





