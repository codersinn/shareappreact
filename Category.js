import React from 'react';
import { StyleSheet, Image, ImageBackground, BackHandler, FlatList, Text, View, TouchableHighlight, TouchableWithoutFeedback, SafeAreaView, Dimensions ,TouchableOpacity ,TextInput } from 'react-native';
import Amplify from 'aws-amplify';
import { Auth } from 'aws-amplify';
import awsConfig from './Src/aws-exports';
import { Col } from 'native-base';
import Tabbar from 'react-native-tabbar-bottom'
//var SharedPreferences = require('react-native-shared-preferences');.
import AsyncStorage from '@react-native-community/async-storage';
const DEVICE_HEIGHT = Dimensions.get('window').height -100;
import { MaterialIndicator } from 'react-native-indicators';
import Autocomplete from 'react-native-autocomplete-input';
import SideMenu from 'react-native-side-menu';
import Menu from './Menu';

Amplify.configure(awsConfig);

Auth.currentAuthenticatedUser({
  bypassCache: true  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
}).then(user => {

  // this.storeItem("username",JSON.stringify(user.username))
  // this.storeItem("email",JSON.stringify(user.signInUserSession.idToken.payload.email))
  // this.storeItem("key","value")
  // this.storeItem("event_id",JSON.stringify(user.signInUserSession.idToken.payload.event_id))
  // this.storeItem("phone_number",JSON.stringify(user.signInUserSession.idToken.payload.phone_number))
  // this.storeItem("client_id",JSON.stringify(user.signInUserSession.accessToken.payload.client_id)) 

  email = user.signInUserSession.idToken.payload.email

}

)
  .catch(err => {

    // SharedPreferences.setItem("username",JSON.stringify(user.username))
    // SharedPreferences.setItem("email",JSON.stringify(user.signInUserSession.idToken.payload.email))
    // SharedPreferences.setItem("key","value")
    // SharedPreferences.setItem("event_id",JSON.stringify(user.signInUserSession.idToken.payload.event_id))
    // SharedPreferences.setItem("phone_number",JSON.stringify(user.signInUserSession.idToken.payload.phone_number))
    // SharedPreferences.setItem("client_id",JSON.stringify(user.signInUserSession.accessToken.payload.client_id))


  })

import { withAuthenticator, PhoneField } from 'aws-amplify-react-native';


import { getHistoryRequest, getUserRewardsPointsRequest, getCategoryRequest,getSubCategoryRequest ,getSearchBusinessRequest} from './actions'
import { connect } from 'react-redux';
import ProfileScreen from './Component/ProfileScreen'
import { ScrollView } from 'react-native-gesture-handler';
import DrawerLayout from 'react-native-gesture-handler/DrawerLayout';

var email = 'include@gmail.com';
var phone = '123456789';
var username = "username"
var data = []
const arrayEarnRewards = [

  {
    name: 'Reviews',
    img: require('./img/reviewscategory.jpg')
  },
  {
    name: 'like,\ncomment,\nshare',
    img: require('./img/socialmediacategory.jpg')
  },
  {
    name: 'IG Post',
    img: require('./img/instagramselfie.jpg')
  },
  {
    name: 'Referrals',
    img: require('./img/referralscategory.jpg')
  },
  {
    name: 'Check In',
    img: require('./img/fbcheckin.jpg')
  },
  {
    name: 'memorabilla',
    img: require('./img/memorabilla.jpg')
  },

]
const arrayShop = [

  {
    name: '1 Free\nSession',
    img: require('./img/freetshirt.jpg'),
    points: '750 Points'
  },
  {
    name: '10% off\n1 month',
    img: require('./img/yogamat.jpg'),
    points: '200 Reward Points'

  },
  {
    name: '10% off\n3 month',
    img: require('./img/tendiscount.jpg'),
    points: '500 Reward Points'

  },
  {
    name: 'branded\nclothing',
    img: require('./img/freepersonalt.jpg'),
    points: '900 Points'

  },
  {
    name: 'Meal Plan',
    img: require('./img/freemealplan.jpg'),
    points: '500 Points'

  },
  {
    name: 'unlimited for\n30 days',
    img: require('./img/freemealplan.jpg'),
    points: '1250 Points'

  },

]
class AppScreen extends React.Component {

  // static navigationOptions = {
  //   header: null,
  // };

  constructor() {
    super()
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.getdataFromSharedPreference();
    this.state = {
      animating: false,
      data: [],
      token: '',
      pageType: "Category",
      searchBusinessArray:[],
      isOpen:false
    }
  }



  componentWillMount() {
    this.props.getCategoryRequest();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  getdataFromSharedPreference() {

    try {

      AsyncStorage.getItem('email', (err, result) => {
        email = result;

      });
      AsyncStorage.getItem('phone_number', (err, result) => {
        phone = result;
        console.log(phone);
      });
      AsyncStorage.getItem('username', (err, result) => {
        username = result;
        console.log(username);
      });

    }
    catch (e) {
      console.error(e.message);
    }

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    //alert('hello');
    BackHandler.exitApp();
    //this.props.navigation.goBack(null);
    return true;
  }



 

  _renderSubCategoryItem(item) {
    return (
      <TouchableHighlight
         onPress={()=>{
           this.props.navigation.navigate("App",{pageType:'HomeScreen',head:item.name})
         }}
      >
      <View style={{ flexDirection: 'row',padding:10,alignItems:'center' }}>

        <Image
          style={{ width: 100, height: 70 }}
          source={{ uri: item.image }}
        />

        <Text style={{fontSize:25,fontFamily:'Utsaah',marginLeft:20}}>{item.name}</Text>
      </View>
      </TouchableHighlight>
    )
  }

  _categoryRenderItem(item) {

    return (
      <Col style={{ padding: 1, paddingLeft: 0, }}>

        <ImageBackground
          source={{ uri: item.image }}
          style={{ flex: 1, resizeMode: 'cover',height: DEVICE_HEIGHT/3 }}>
          <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.5)', width: '100%', flex: 1 }}>

            <TouchableHighlight onPress={() => {
              // this.props.navigation.navigate('Details',{item:item})
              // this.setState({ pageType: 'SubCategory' })
              // this.props.getSubCategoryRequest({
              //   category_id: item.id
              //     })
                  this.props.navigation.navigate('SubCategory', { item: item })

            }}>
              <Text style={{ color: 'white', fontSize: 20, fontFamily: 'Hobo Std' }}>{item.name}</Text>
            </TouchableHighlight>
          </View>

        </ImageBackground>

      </Col>

    )
  }


  toggleDrawer() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  _headerBar = () => {
    return (
      <View style={styles.headerView}>
        <TouchableWithoutFeedback onPress={() => this.toggleDrawer()}>
          <View style={{ width: 50, height: 60, backgroundColor: 'white', zIndex: 1 }}>
            <Image
              style={{ width: 30, height: 30, marginTop: 15 }}
              source={{ uri: 'https://cdn4.iconfinder.com/data/icons/yellow-commerce/100/.svg-19-512.png' }}
            />
          </View>
        </TouchableWithoutFeedback>
        {this.state.pageType =='Category' && this.getCategoryHeader()}
        {(this.state.pageType !=='Category') && this.getNormalHeader()}
      </View>)
  }

 


  getNormalHeader(){
    return(
      <Text style={{ color: 'black',  fontSize: 20,marginLeft:-50, fontWeight: 'bold', textAlign: 'center', width: '100%' }}>Shop</Text>

    )
  }

  getSearchItem(item){
    return(
      <TouchableOpacity onPress={()=>{
        this.setState({searchBusinessArray:[]})
      
        this.props.navigation.navigate('App', { item: item })
      }}>
      <Text style={{ color: 'black',  fontSize: 20, fontWeight: 'bold', textAlign: 'center', width: '100%' }}>{item.item.name}</Text>
</TouchableOpacity>
    )
  }

  getCategoryHeader(){
    console.log('this.props.searchBusinessArray',this.props.searchBusinessArray);
    
    return(
      <View style={{
        borderBottomLeftRadius: 20,
        flexDirection: 'row',
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        height: 40,
        flex:1,
       marginRight:20,
       paddingTop:10,
       paddingLeft:10,
        backgroundColor: '#4c4c4c4c', width: '70%'
      }}>

<TextInput
style={{color:'#4c4c4c',width:'90%',marginTop:-10}}
      
      onChangeText={text =>{ this.setState({ query: text })
        this.props.getSearchBusinessRequest({'search_string':text})
      }}
      placeholder="enter business name"

    />
        <Image
          style={{ width: 20, height: 20, }}
          source={require('./img/search_new.png')}

        />
      </View>
     
    )
  }

  getItemByType(type, item) {

    var view = {
     
      'Category': type == 'Category' && this._categoryRenderItem(item.item),
      'SubCategory': type == 'SubCategory' && this._renderSubCategoryItem(item.item),
      'default': 'Default item'
    };
    return (view[type] || view['default']);
  }

  getArrayByType(type) {
    var array = {
      
      'Category': this.props.categorydata,
      'SubCategory': this.props.subCategorydata,
      'default': []
    };
    return (array[type] || array['default']);
  }

  getColumnType(type) {
    var columnType = {
      'HomeScreen': 2,
      'ProfileScreen': 2,
      'Category': 2,
      'SubCategory': 1,
      'NotificationScreen': 1,
      'default': 1
    };
    return (columnType[type] || columnType['default']);
  }

  componentWillReceiveProps(newprops){
if (this.props.searchBusinessArray!=newprops.searchBusinessArray) {
  this.setState({searchBusinessArray:newprops.searchBusinessArray})
}
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  onMenuItemSelected = item =>
    this.setState({
      isOpen: false,
      selectedItem: item,
    });


  render() {
    const columntype = this.getColumnType(this.state.pageType);
    const menu = <Menu onItemSelected={this.onMenuItemSelected} type='category'      navigation={this.props.navigation}
    />;

    return (
      <SideMenu
        menu={menu}
        isOpen={this.state.isOpen}
        onChange={isOpen => this.updateMenuState(isOpen)}
      >
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>


        <View style={styles.container}>

          {this._headerBar()}
          {this.props.loading && <MaterialIndicator color="#5B5A5F" size={30} trackWidth={2} />}
         
        

<ScrollView style={{flex:1,height:'100%'}}>
<ImageBackground style={{width:'100%'}}>
        
         
          {(this.state.pageType === "Category" ||this.state.pageType === "SubCategory" || this.state.pageType === "HomeScreen" || this.state.pageType === 'ProfileScreen' || this.state.pageType === "NotificationScreen" || this.state.pageType === 'ChatScreen') &&
            <FlatList
              style={{ flex:1,height:DEVICE_HEIGHT,}}

              renderItem={(item, index) => this.getItemByType(this.state.pageType, item)}
              key={this.state.pageType}
              data={this.getArrayByType(this.state.pageType)}
              numColumns={columntype}
              bounces={false}
            />}

{ (this.state.searchBusinessArray.length >0) &&
          <FlatList
              style={{flexGrow:0,  position: 'absolute',
    backgroundColor:'#fff',width:'100%'}}

              renderItem={(item, index) => this._renderSubCategoryItem(item.item)}
             
              data={this.props.searchBusinessArray}
              numColumns={1}
              bounces={false}
            />}
       </ImageBackground>

</ScrollView>
         

          {this.state.pageType === "SearchScreen" &&

            <ProfileScreen
              email={email}
              phone={phone}
              username={username}
              navigation={this.props.navigation}
            />

          }

           {this.state.pageType !== "Category" &&
            <Tabbar
              tabbarBgColor="#fff"
              selectedIconColor="#5372C4"
              stateFunc={(tab) => {
                this.setState({ pageType: tab.page })
                console.log(tab.pageType);
                if (tab.page == 'NotificationScreen') {
                  // alert(tab.page);
                  this.props.getUserRewardsPointsRequest({
                    email: null
                  });
                  //  this.call()
                }
                else if (tab.page == 'ChatScreen') {

                  let requestBody = {
                    email: 'include@gmail.com'
                  }
                  //debugger;
                  this.props.getHistoryRequest(requestBody);
                }

                //this.props.navigation.setParams({tabTitle: tab.title})
              }}
              activePage={this.state.pageType}

              tabs={[
                {
                  page: "Category",
                  icon: "home",
                },
                {
                  page: "NotificationScreen",
                  icon: "paper"
                },
                {
                  page: "ProfileScreen",
                  icon: "cart",
                },
                {
                  page: "ChatScreen",
                  icon: "reorder",
                },
                {
                  page: "SearchScreen",
                  icon: "settings",
                },
              ]}
            />
          }
        </View>
       
      </SafeAreaView>
      </SideMenu>
    );
  }
}


const mapStateToProps = (state) => {  
  
  return {
    loading: state.loading,
    getRewardsData: state.getRewardsData,
    rewardsPointsData: state.rewardsPointsData,
    categorydata: state.categoryArray,
    subCategorydata: state.subCategoryArray,
    searchBusinessArray: state.searchBusinessArray,
  }
};

const mapDispatchToProps = {
  getHistoryRequest,
  getUserRewardsPointsRequest,
  getCategoryRequest,
  getSubCategoryRequest,
  getSearchBusinessRequest
};

const Category = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppScreen);

export default Category;
//export default withAuthenticator(App);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  headerView: {
    backgroundColor: 'white', alignItems: 'center', width: '100%', flexDirection: 'row', height: 60,
    justifyContent: 'flex-start',
  }
});





