import React, { Component } from 'react';
import { ActivityIndicator, View,Easing, Animated,Text,TouchableOpacity, StyleSheet ,ImageBackground,Image,TextInput,Button} from 'react-native';
import logoImg from '../img/light_blur.jpg';
import blackback_arrow from '../img/blackback_arrow.png';
import search_icon from '../img/search_icon.png';
import loadingicon from '../img/username.png';
import CardView from 'react-native-cardview'
import UserInput from './UserInput';
class UselessTextInput extends Component {
   render() {
     return (
       <TextInput
         {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
         editable = {true}
         maxLength = {40}  
       />
     );
   }
 }
class ActivityIndicatorExample extends Component {
   state = { animating: true,text: 'Useless Placeholder' }
   
   closeActivityIndicator = () => setTimeout(() => this.setState({
   animating: false }), 4000)
 
	
constructor () {
 
    super()
 
    this.RotateValueHolder = new Animated.Value(0);
 
  }


  componentDidMount() {
 
   this.StartImageRotateFunction();

 }

StartImageRotateFunction () {

 this.RotateValueHolder.setValue(0)
 
 Animated.timing(
   this.RotateValueHolder,
   {
     toValue: 1,
     duration: 3000,
     easing: Easing.linear
   }
 ).start(() => this.StartImageRotateFunction())

}
   componentDidMount() { 
      this.StartImageRotateFunction();
      this.closeActivityIndicator()}
   render() {
      const animating = this.state.animating
      const RotateData = this.RotateValueHolder.interpolate({
         inputRange: [0, 1],
         outputRange: ['0deg', '360deg']
       })
      return (

         <View style={{flex: 1}}>

<View style={{flex: 1}}>
         <View style = {{flex: 1,
     
            justifyContent: 'center',
            alignItems: 'center',}}>

     

{/* <View  style={{  flex: 1,
    
    minWidth:'100%',
    }}>

    <View
    style={{marginLeft:10,
      justifyContent: 'flex-start', marginTop:10,flexDirection: 'row',}}
    >

<Image source={search_icon} style={{width:25,height:25}}/>
<Text style={{marginLeft:10,marginTop:5}}>Search</Text>
</View>
<View
  style={{
    
     minWidth:'100%',
height:1,
     backgroundColor:'#B8B8B8',
     marginTop:10
   
  }}
/>
</View> */}

<View
style={{
    
   minWidth:'100%',

  alignItems:'center',
   marginTop:10
 
}}>

<Text style={{fontWeight:'bold',
        fontSize:12}}>TOP PEOPLE</Text>
</View>

<CardView
style={{width:280,margin:10,alignItems:'center',height:300}}
          cardElevation={2}
          cardMaxElevation={2}
          cornerRadius={5}>

         

<Image source={logoImg} style={{height:100,justifyContent:'center'}} />


<Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png'}}
              style={{width: 79, height: 79, borderRadius: 150/2,marginTop:-37}} />

     <Text style={{fontWeight:'bold',
        fontSize:19,color:'#383838',marginTop:10}}>No User Found</Text>

<TextInput
            style={styles.input}
            value={this.state.value}
            onChangeText={text=>this.setState({value:text})}
            multiline={true}
            placeholder="Please enter"
            underlineColorAndroid='transparent'
    />

          
</CardView>

<View style = {{ flex: 1,
     
     alignItems: 'center',
     marginTop: 10}}>

<Text>CONVERSATION</Text>



   


</View>


{ this.state.animating && 
  
   <Animated.Image
   style={{
     width: 100,
     marginBottom:55,
     height: 100,  
     
     transform: [{rotate: RotateData}] }}

     source={loadingicon} />

   
 }
 

         </View>


         </View>
         <Button title="Submit"
  color="#5372C4" style={{height:80}} />
      </View>
      )
   }
}
export default ActivityIndicatorExample

const styles = StyleSheet.create ({

   container: {
      flex: 1,
     
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20
   },
   activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80
   },input: {
      width:200,
      height:70,
      marginTop:10,
      borderColor:'#5372C4',
      borderWidth:1,
      textAlignVertical: 'top'
      
  },
})