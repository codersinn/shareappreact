import React from 'react'
import {
  StyleSheet,
  View,
  AppState,
  ActivityIndicator,
} from 'react-native'

// AWS Amplify modular import
import Auth from '@aws-amplify/auth'

export default class AuthLoadingScreen extends React.Component {
  state = {
    userToken: false
  }
   componentDidMount () {
     this.loadApp()

     
  }
  
  // Get the logged in users and remember them
  loadApp () {
     Auth.currentAuthenticatedUser()
    .then(user => {

      this.setState({userToken: user.signInUserSession.accessToken.jwtToken})

      this.props.navigation.navigate('App')
 
    })
    .catch(err =>  this.props.navigation.navigate( 'SignInScreen')
    )



    

   
     }
  render() {
    return (
      <View style={styles.container}> 
        <ActivityIndicator size="large" color="#fff" />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
