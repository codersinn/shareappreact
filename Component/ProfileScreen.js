import React, { Component } from 'react';
import { ActivityIndicator, View,Easing, Animated,TouchableWithoutFeedback,Text,TouchableOpacity, StyleSheet ,ImageBackground,Image,TextInput,Button} from 'react-native';
import blackback_arrow from '../img/blackback_arrow.png';
import search_icon from '../img/search_icon.png';
import loadingicon from '../img/username.png';
import CardView from 'react-native-cardview'
import UserInput from './UserInput';
import backarrow from '../img/backarrow.png';
import logoImg from '../img/light_blur.jpg';
import Auth from '@aws-amplify/auth'


//var SharedPreferences = require('react-native-shared-preferences');
var email = 'hi';
var phone = 'hi';

class ProfileScreen extends Component {
   state = { animating: true,text: 'Useless Placeholder' }
   
constructor (props) {
 
    super(props)
      
  }

  componentDidMount(){
        

 }
 async storeItem(key, item) {
   try {
       //we want to wait for the Promise returned by AsyncStorage.setItem()
       //to be resolved to the actual value before returning the value
        await AsyncStorage.setItem(key, item);
       
   } catch (error) {
     console.log(error.message);
   }
 }
async onclick()
{ 
  this.props.navigation.navigate("SignInScreen")
   Auth.signOut();
}


 
   render() {
    
      return (
         

         <View style={{flex: 1,width:'100%'}}>
{/* 
<View style={{ backgroundColor: 'white',alignItems:'center',width:'100%',flexDirection:'row',height:60,
       justifyContent:'flex-start'}}>
      

      <TouchableWithoutFeedback onPress={() => this.props.navigation.toggleDrawer() }>
        <View style={{width:100,height:60,backgroundColor:'white',zIndex:1}}>
       
        <Image
         style={{width: 30, height: 30,marginTop:15}}
         source={{uri: 'https://cdn4.iconfinder.com/data/icons/yellow-commerce/100/.svg-19-512.png'}}
       />
     
     </View>
     </TouchableWithoutFeedback>
       <Text style={{ Color: 'black',marginLeft:-120,fontSize:20,fontWeight: 'bold',textAlign:'center',width:'100%'}}>Setting</Text>
       </View>
 */}

<View style={{flex:1}}>

<View style={{flexDirection:'column',alignSelf:'center',justifyContent:'center',width:'100%',marginTop:15}}> 
             <Text style={{color:'#B8B8B8',fontSize:15,fontWeight:'bold',textAlign:'center'}}>Username</Text>
             <Text style={{color:'#5372C4',fontSize:15,fontWeight:'bold',textAlign:'center'}}>{this.props.username}</Text>
             
             </View>



<View style={{flexDirection:'column',alignSelf:'center',justifyContent:'center',width:'100%',marginTop:15}}> 
             <Text style={{color:'#B8B8B8',fontSize:15,fontWeight:'bold',textAlign:'center'}}>Email</Text>
             <Text style={{color:'#5372C4',fontSize:15,fontWeight:'bold',textAlign:'center'}}>{this.props.email}</Text>
             
             </View>


             <View style={{flexDirection:'column',alignSelf:'center',justifyContent:'center',width:'100%',marginTop:15}}> 
             <Text style={{color:'#B8B8B8',fontSize:15,fontWeight:'bold',textAlign:'center'}}>Phone</Text>
             <Text style={{color:'#5372C4',fontSize:15,fontWeight:'bold',textAlign:'center'}}>{this.props.phone}</Text>
             
             </View>

             <View style={{flexDirection:'column',alignSelf:'center',justifyContent:'center',width:'100%',marginTop:15}}> 
             <Text style={{color:'#5372C4',fontSize:15,fontWeight:'bold',textAlign:'center'}}>Contact Us</Text>

             </View>

             <View style={{flexDirection:'column',alignSelf:'center',justifyContent:'center',width:'100%',marginTop:15}}> 
             <Text style={{color:'#5372C4',fontSize:15,fontWeight:'bold',textAlign:'center'}}>Cancel Account</Text>

             </View>

             <View style={{flexDirection:'column',alignSelf:'center',justifyContent:'center',width:'100%',marginTop:15}}> 
             <Text style={{color:'#5372C4',fontSize:15,fontWeight:'bold',textAlign:'center'}}>terms and conditions</Text>

             </View>

             <View style={{flexDirection:'column',alignSelf:'center',justifyContent:'center',width:'100%',marginTop:15}}> 
             <Text style={{color:'#5372C4',fontSize:15,fontWeight:'bold',textAlign:'center'}}>privacy policy</Text>

             </View>



            

             </View>

<View style={{flex:2, width:'100%',position: 'absolute',alignItems:'center',justifyContent:'center',
  bottom:0}}>
{ 
   

  }
  <TouchableOpacity
                    onPress={() => this.onclick()}
                    style={styles.buttonStyle}>
<Text style={{color:'white',backgroundColor:'#5372C4',fontSize:15,fontWeight:'bold',marginBottom:55,textAlign:'center',alignSelf:'center'}}>Sign Out</Text>
</TouchableOpacity>
</View>

      </View>
      )
   }
}
export default ProfileScreen

const styles = StyleSheet.create ({

   container: {
      flex: 1,
     
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20
   },
   button: {
      backgroundColor: '#5372C4',
      borderColor: '#5372C4',
      borderWidth: 5 ,
      height:0     
   },
   buttonStyle: {
      alignItems: 'center',
      backgroundColor: '#5372C4',
      padding: 10,
      margin:0,
      marginBottom: 0,
      borderRadius: 3,
    },

   activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80
   },input: {
      width:200,
      height:70,
      marginTop:10,
      borderColor:'#5372C4',
      borderWidth:1,
      textAlignVertical: 'top'
      
  },
})