import React ,{Component} from 'react'
import {StyleSheet,Text,View,Image ,TouchableWithoutFeedback,ImageBackground,TouchableHighlight,SafeAreaView} from 'react-native'

import logoImg from '../img/reviewscategory.jpg';
import leftarrow from '../img/righticon.png';
import backarrow from '../img/backarrow.png';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ReviewonGooglelogoImg from '../img/reviewgoogle.jpg';
import ReviewonFaceBooklogoImg from '../img/reviewfb.jpg';

export default class Details extends Component{

  
    constructor(props){
        super(props)       
    }

    componentDidMount(){
     // this.props.navigation.navigate('SignInScreen')

    }
    render(){
      const item=this.props.navigation.getParam('item', 'NO-ID')
        return(
          <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={styles.container}>
            <ImageBackground
                     
             source={logoImg} style={styles.image} >
             
             <View   style={{backgroundColor: 'rgba(52, 52, 52, 0.8)',width:'100%',flex:1,alignItems:'flex-start'}}>

             <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
<Image source={backarrow} style={{width:25,height:25,margin:10}} />
</TouchableHighlight>

<Text style={styles.centertext}>{item.name}</Text>

             </View>


            
             </ImageBackground>
             
             <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CommentOnFacebookPost',{title:'Review on Google',image:ReviewonGooglelogoImg,points:'50',desc: 'hey, did you just leave us a review? \n \n thats awesome! to receive points. \n please press the button below'})}> 
            <View  style={styles.mainview}>
            
            <View style={{flex:1}}>
      
      
        <Text style={styles.title}>Review on Google</Text>
       
        <Text style={styles.titleshort}>50 Points</Text>
        </View>    
        <Image source={leftarrow} style={styles.shortimage} ></Image>   
      </View>
      </TouchableWithoutFeedback>

      <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CommentOnFacebookPost',{title:'Review on Yelp',image:ReviewonGooglelogoImg,points:'50',desc: 'hey, did you just leave us a review? \n \n thats awesome! to receive points. \n please press the button below'})}> 
          
      <View
        style={styles.mainview}>
        <View style={{flex:1}} >
        <Text style={styles.title}>Review on Yelp</Text>
        <Text style={styles.titleshort}>50 Points</Text>
        </View>    
        <Image source={leftarrow} style={styles.shortimage} ></Image>        
       
      </View>
      
</TouchableWithoutFeedback>

<TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CommentOnFacebookPost',{title:'Review on Facebook',image:ReviewonFaceBooklogoImg,points:'50',desc:'hey, did you just leave us a review? \n \n thats awesome! to receive points. \n please press the button below'})}> 
     
      <View style={styles.mainview}>
        <View style={{flex:1}} >
        <Text style={styles.title}>Review on Facebook</Text>
        <Text style={styles.titleshort}>50 Points</Text>
        </View>    
        <Image source={leftarrow} style={styles.shortimage} ></Image>        
       
      </View>

      </TouchableWithoutFeedback>
          </View>
          </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    mainview:{
        flexDirection: 'row',
        height: 40,
        marginTop:20,
        marginLeft:20,
        padding: 5,
        margin:2,
       
    },
    container:{
        backgroundColor:'#ffffff',
        flex:1,    
        alignItems:'center',
        justifyContent:'flex-start',
    },
    image: {
        width:'100%',
        height: 250,
        
      },
      shortimage: {
        width:15,
        marginTop:5,
        height: 15,
        marginLeft:5,
        alignItems: 'center'
      },
    title:{
        fontWeight:'bold',
        fontSize:14
    },
    titleshort:{
        color:'#B8B8B8',
        fontSize:10,

    },
    centertext:{
      color:'#ffffff',
      fontSize:20,
      marginLeft:'40%',
      marginTop:'20%'
     
  }
})