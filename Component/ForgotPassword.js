import React from 'react'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import loadingicon from '../img/loadinglogo.png';
import backarrow from '../img/blackback_arrow.png';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
  TouchableHighlight,
  Easing,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  Keyboard,
  Alert,
  Animated,
} from 'react-native'


import {
  Container,
  Item,
  Input
} from 'native-base'

// AWS Amplify modular import
import Auth from '@aws-amplify/auth'

// Load the app logo

export default class ForgotPassword extends React.Component {


  constructor(props){
    super(props)    
    this.RotateValueHolder = new Animated.Value(0);
   
     
}
  state = {
    username: '',
    password: '',
    newPassword:'',
    authCode:'',
    code:'',
    fadeIn: new Animated.Value(0),
    fadeOut: new Animated.Value(0),  
    isHidden: false,
    loginui: true,
    load: false
  }
  componentDidMount() {
    this.fadeIn()
  }
  fadeIn() {
    Animated.timing(
      this.state.fadeIn,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }
    ).start()
    this.setState({isHidden: true})
  }
  fadeOut() {
    Animated.timing(
      this.state.fadeOut,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }
    ).start()
    this.setState({isHidden: false})
  }
  onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }


  // Sign in users with Auth
  async signIn() {
   
    const { username, password } = this.state
    this.setState({load:true})
    await Auth.signIn(username, password)
    .then(user => {
      this.setState({ user })
      this.setState({loginui: false,load : false})
      //this.props.navigation.navigate('Authloading')
    })
    .catch(err => {
      if (! err.message) {
        Alert.alert(
          
          err,
          [
            { text: 'OK', onPress: ()=>{
  
              this.setState({load:false});
            }
           }
          ],
          { cancelable: false }
        )
        console.log('Error when signing in: ', err);
      } else {
        Alert.alert(
          
          err,
          [
            //{text: 'OK', onPress: () => this.doRefreshState()},
            { text: 'OK', onPress: ()=>{
  
              this.setState({load:false});
  
            } }
          ],
          { cancelable: false }
        )
        console.log('Error when signing in: ', err);
      }
    })
  }

   
 StartImageRotateFunction () {
 
  this.RotateValueHolder.setValue(0)
  
  Animated.timing(
    this.RotateValueHolder,
    {
      toValue: 1,
      duration: 17000,
      delay:100,
      easing: Easing.linear
    }
  ).start(() => this.StartImageRotateFunction())
 
 }

  
  // ConfirmSign in users with Auth
  async confirmSign() {
  
    this.setState({load:true})
    await Auth.confirmSignIn(this.state.user,  this.state.code,'SMS_MFA')
    .then(user => {
      //Alert.alert('Error when signing in: ', JSON.stringify(user))
      this.setState({load:false})
    })
    .catch(err => {
      if (! err.message) {


        this.setState({load:false});

        setTimeout(function(){
          Alert.alert('Error when signing in: ', err)     
        }, 200);

      } else {

        this.setState({load:false});

        setTimeout(function(){
          Alert.alert('Error when signing in: ', err.message)     
        }, 200);
    
      }
    })
  }
 // Request a new password
 async forgotPassword() {
    this.setState({load:true})
    const { username } = this.state
    await Auth.forgotPassword(username)
    .then(data => {
        this.setState({loginui: false,load : false})
     
        console.log('New code sent', data)
    })
    .catch(err => {
      if (! err.message) {

        this.setState({load:false});

        setTimeout(function(){
          Alert.alert('Error when Forgot Password ', err.message)     
        }, 200);

      
      } else {

        this.setState({load:false});

        setTimeout(function(){
          Alert.alert('Error when Forgot Password ', err.message)     
        }, 200);
      
      }
    })
  }
  // Upon confirmation redirect the user to the Sign In page
  async forgotPasswordSubmit() {
    this.setState({load:true})
    const { username, authCode, newPassword } = this.state
    await Auth.forgotPasswordSubmit(username, authCode, newPassword)
    .then(() => {
      this.setState({load:false})
      this.props.navigation.navigate('SignInScreen')
      console.log('the New password submitted successfully')
    })
    .catch(err => {
      if (! err.message) {

        this.setState({load:false});
        setTimeout(function(){
          Alert.alert(err)     
        }, 200);


      } else {


        this.setState({load:false});

        setTimeout(function(){
 
          //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
          Alert.alert(err);
     
        }, 200);
       
      }
    })
  }

  render() {
   
    let { fadeOut, fadeIn, isHidden } = this.state;
    const RotateData = this.RotateValueHolder.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
   // this.StartImageRotateFunction();
    return (
      <SafeAreaView style={styles.container}>
       <OrientationLoadingOverlay
          visible={this.state.load}  
          >
           <View>
          <Image  
    source={require('../img/loadlogo.gif')}    
    style={{width: 80, height: 80}}/>            
              </View>
          </OrientationLoadingOverlay>
        <StatusBar/>
       
          <TouchableWithoutFeedback 
            style={styles.container} 
            onPress={Keyboard.dismiss}>
             <KeyboardAvoidingView     
              style={styles.container}   
                behavior="padding">

{ this.state.loginui && 

   
            <View style={styles.container}>
              <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
<Image source={backarrow} style={{width:25,height:25,margin:10}} />
</TouchableHighlight>
                         
                         <View style={{flexDirection: 'column'}}>
                   <Text style={{fontSize: 22,margin:10,
    fontWeight: 'bold',color:'#000'}}> Forgot Password </Text> 
              
                  </View>             
              <Container style={styles.infoContainer}>

                <View style={styles.container}>
                   
                        
                <Text style={styles.itemText} >Username*</Text>            
                                      
                <Item style={styles.itemText}>
                    <Input
                      style={styles.input}
                      placeholder='Enter your uername'
                      placeholderTextColor='#adb4bc'
                      keyboardType={'email-address'}
                      returnKeyType='next'
                      autoCapitalize='none'
                      ref='FirstInput'
                      autoCorrect={false}
                      onSubmitEditing={(event) => {this.refs.FirstInput._root.focus()}}
                      onChangeText={value => this.onChangeText('username', value)}
                      onFocus={() => this.fadeOut()}
                      onEndEditing={() => this.fadeIn()}
                    />                   
                   </Item>
                
                 
                
                 <TouchableOpacity
                    onPress={() => this.forgotPassword()}
                    style={styles.buttonStyle}>
                         <Text style={styles.buttonText}>
                        Send
  </Text>
           
           
                  </TouchableOpacity>   


                  
                 <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('SignInScreen')}
                    style={styles.textViewStyle}>
   <Text style={{ fontSize: 18,
    fontWeight: 'bold',
    color: "#5372C4",}}></Text>

                      
                  </TouchableOpacity>               


                </View>
              </Container>
            </View>
}

{ !this.state.loginui &&
  <View style={styles.container}>
  <TouchableHighlight 
    onPress={() =>   this.setState({loginui: true})}>
<Image source={backarrow} style={{width:25,height:25,marginLeft:10}} />
</TouchableHighlight>
                         
                         
  <Container style={styles.infoContainer}>
    <View style={styles.container}>

    <View style={{flexDirection: 'column'}}>
                   <Text style={{fontSize: 22,margin:10,
    fontWeight: 'bold',color:'black'}}>Forgot Password </Text> 
              
                  </View> 
                  <Text style={styles.itemText}>Confirmation Code*</Text>                   
    <Item style={styles.itemStyle}>
                    <Input
                      style={styles.input}
                      placeholder='Enter your confirmation code'
                      placeholderTextColor='#adb4bc'
                      returnKeyType='go'
                      autoCapitalize='none'
                      autoCorrect={false}
                      secureTextEntry={false}                     
                      onChangeText={value => this.onChangeText('authCode', value)}
                      onFocus={() => this.fadeOut()}
                      onEndEditing={() => this.fadeIn()}
                    />
                  </Item>

                  <Text style={styles.itemText}>Password*</Text>                   
    <Item style={styles.itemStyle}>
                    <Input
                      style={styles.input}
                      placeholder='Enter New password'
                      placeholderTextColor='#adb4bc'
                      returnKeyType='go'
                      autoCapitalize='none'
                      autoCorrect={false}
                      secureTextEntry={false}                     
                      onChangeText={value => this.onChangeText('newPassword', value)}
                      onFocus={() => this.fadeOut()}
                      onEndEditing={() => this.fadeIn()}
                    />
                  </Item>


                 
                  <TouchableOpacity
                    onPress={() => this.forgotPasswordSubmit()}
                    style={styles.buttonStyle}>
                    <Text style={styles.buttonText}>
                    Submit  
                    </Text>
                  </TouchableOpacity>


                     
                 <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('SignInScreen')}
                    style={styles.textViewStyle}>
   <Text style={{ fontSize: 18,
    fontWeight: 'bold',
    color: "#5372C4",}}></Text>

                      
                  </TouchableOpacity>   
                </View>
              </Container>
            </View>
}
 </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
       
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop : 10,
    backgroundColor: '#FFFFFF',
    flexDirection: 'column'
  },
  input: {
    height:60,
    borderWidth: 1,
    margin: 10,
    fontSize: 17,
    fontWeight: 'bold',
    color: '#000',
  },
 
  itemStyle: {
    marginBottom: 20,
  },
  itemText: {
    marginBottom: 3,
    marginStart:14
  },
  iconStyle: {
    color: '#fff',
    fontSize: 30,
    marginRight: 15
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#5372C4',
    padding: 14,
    margin:14,
    marginBottom: 20,
    borderRadius: 3,
  },

  textViewStyle: {
    alignItems: 'center',
    backgroundColor: '#ffffff',
    padding: 14,
    margin:14,
    marginBottom: 20,
    borderRadius: 3,
  },


  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "#fff",
  },
  logoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 400,
    bottom: 180,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
})