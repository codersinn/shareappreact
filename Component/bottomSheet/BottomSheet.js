import React from "react";
import { Dimensions, Image, Text, TouchableHighlight, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Modal from "./ModalBox";


const { height } = Dimensions.get("window");
var screenWidth = Dimensions.get("window").width;


/*
      Bottom Sheet: Open when click on the size and quantity.
    */

class BottomSheet extends React.PureComponent {
    state = {
        selectedItem: "1",
        data: []
    };

    renderSeparator = () => {
        return <View style={styles.lineSaperator} />;
    };

    renderItemView = item => {
        if (this.state.selectedItem == item.name && true) {
            return (
                <View style={styles.collomDirenction} key={item.key}>
                     <View
                            style={
                                [
                                    styles.countryStyle,
                                    {
                                        flexDirection: 'row',
                                        alignItems: 'center',

                                    }
                                ]
                            }>

                            <Text style={{ fontSize: 45, marginRight: 15 }}>
                                {item.flag}
                            </Text>
                            <Text style={{ fontSize: 20, color: '#000' }}>
                                {item.name} ({item.dial_code})
                                    </Text>

                                    <Image
                            style={styles.rightImageStyle}
                            source={require("./assests/check.png")}
                        />
                        </View>

                       
                       

                    {this.renderSeparator()}
                </View>
            );
        } else if (item.name && true) {
            return (
                <TouchableHighlight
                    style={styles.whtitebackground}
                    underlayColor="rgb(225,225,235)"
                    key={item.key}
                    onPress={() => {
                        this.setState({
                            selectedItem: item.name
                        });

                        /**
                         * selectedIetm callback@{} method notify which value is selected
                         */
                        this.props.selectedIetm(item);
                        this.hideSizeSheet();
                    }}
                >
                    <View style={styles.collomDirenction}>
                        <View
                            style={
                                [
                                    styles.countryStyle,
                                    {
                                        flexDirection: 'row',
                                        alignItems: 'center',

                                    }
                                ]
                            }>

                            <Text style={{ fontSize: 45, marginRight: 15 }}>
                                {item.flag}
                            </Text>
                            <Text style={{ fontSize: 20, color: '#000' }}>
                                {item.name} ({item.dial_code})
                                    </Text>


                        </View>

                        {this.renderSeparator()}
                    </View>
                </TouchableHighlight>
            );
        }
    };

    /*
        Open the bottom Sheet
      */
    showSizeSheet(array, selectedValue) {
        this.setState({ data: array, selectedItem: selectedValue });

        this._panel.open();
    }

    /*
        Hide Content Sheet
      */
    hideSizeSheet() {
        this._panel.close();
    }

    /*
        To do: Need to the change the element for the size and quantity
      */
    render() {
        return (
            <Modal
                style={[styles.modal, styles.modal4]}
                position={"bottom"}

                ref={ref => {
                    this._panel = ref;
                }}
            >
                <View style={styles.container}>
                    <Text style={styles.sizeStyle}>{this.props.heading}</Text>
                    <View style={styles.lineSaperator} />
                    <ScrollView style={styles.scrolView} contentContainerStyle={{ paddingBottom: 40 }}>
                        <View style={styles.width}>
                            {this.state.data != null &&
                                this.state.data.map(item => this.renderItemView(item))}
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        );
    }
}

const styles = {
    container: {
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        width: screenWidth
    },
    panel: {
        height: 400,
        backgroundColor: "white",
        position: "relative",
        borderColor: "#e1e1eb",
        borderWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 11
        },
        shadowOpacity: 0.55,
        shadowRadius: 14.78,

        elevation: 22
    },
    width: { width: screenWidth },
    itemSize: {
        width: screenWidth,
        borderBottomColor: "#e1e1eb",
        borderWidth: 1,
        height: 60
    },
    modal: {
        justifyContent: "center",
        alignItems: "center"
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 415
    },
    collomDirenction: {
        flexDirection: "column"
    },
    sizeStyle: {
        fontSize: 16,
        textAlign: "center",
        justifyContent: "center",
        flex: 1,
        height: 75,
        paddingTop: 40,
    },
    whtitebackground: {
        backgroundColor: "white"
    },
    itemViewStyle: {
        flexDirection: "row",
        alignItems: "center",
        height: 59
    },
    textStyleAvail: {
        fontSize: 13,
        marginTop: 7,
        marginLeft: 16,
        flex: 1,
    },
    rightImageStyle: {
      
        height: 20,
        width: 20,
        marginRight: 15
    },
    notifytext: {
        marginRight: 15,
        flex: 1,
    },
    lineSaperator: {
        height: 0.5,
        width: screenWidth,
        backgroundColor: "rgb(153,153,165)"
    },
    outofstockView: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "rgb(244,244,248)",
        height: 59
    },
    outofstockText: {
        fontSize: 13,
        marginTop: 7,
        marginLeft: 16,
        flex: 1,
        color: "rgb(153,153,165)"
    },
    scrolView: { height: 370, width: screenWidth }
};

export default BottomSheet;
