import React ,{Component} from 'react'
import {StyleSheet,Text,View,Image,ImageBackground} from 'react-native'

import logoImg from '../img/splash.png';
export default class Splash extends Component{
    constructor(props){
        super(props)
        setInterval(()=>{
            this.state = ({timer:this.state+1})
        },3000)
    }
    render(){
        return(
            <View style={styles.container}>
            <ImageBackground source={logoImg} style={styles.image} ></ImageBackground>
          </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        flex:1,
        justifyContent:'center',
        alignItems: 'center'
    },
    image: {
        width: 360,
        height:360,
    resizeMode: 'stretch', // or 'stretch',
    justifyContent: 'center',
      },
    title:{
        fontWeight:'bold',
        fontSize:18
    }
})  