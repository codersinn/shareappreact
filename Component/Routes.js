import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';

import Login from './LoginScreen';
import Signup from './LoginScreen';
import main from './main';

export default class Routes extends Component {

	constructor() {
		super()
		this.state = {
			page: "HomeScreen",
		}
	}

	render() {
		return(
			<Router>
			    <Stack key="root" hideNavBar={true}>
			      <Scene key="main" component={main} title="main" initial={true}/>
			      <Scene key="signup" component={Login} title="Register"/>
			    </Stack>
					
			 </Router>
			)
	}
}