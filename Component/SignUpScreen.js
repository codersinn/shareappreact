import React from 'react'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import loadingicon from '../img/loadinglogo.png';
import backarrow from '../img/blackback_arrow.png';
import BottomSheet from './bottomSheet/BottomSheet';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
  Easing,
  StatusBar,
  KeyboardAvoidingView,
  TouchableHighlight,
  Keyboard,
  Alert,
  ScrollView,
  Image,
  Modal,
  FlatList,
  Animated,
} from 'react-native'


import {
  Container,
  Item,
  Input
} from 'native-base'

// AWS Amplify modular import
import Auth from '@aws-amplify/auth'


// Load the app logo


// Import data for countries
import data from './countriesData'


// Default render of country flag
const defaultFlag = data.filter(
  obj => obj.name === 'United States'
)[0].flag

// Default render of country code
const defaultCode = data.filter(
  obj => obj.name === 'United States'
)[0].dial_code

export default class SignUpScreen extends React.Component {


  constructor(props) {
    super(props)
    this.RotateValueHolder = new Animated.Value(0);


  }
  state = {
    username: '',
    password: '',
    email: '',
    phoneNumber: '',
    authCode: '',
    code: '',
    fadeIn: new Animated.Value(0),
    fadeOut: new Animated.Value(0),
    isHidden: false,
    loginui: true,
    load: false,
    flag: defaultFlag,
    modalVisible: false,
  }
  componentDidMount() {
    this.fadeIn()
  }
  fadeIn() {
    Animated.timing(
      this.state.fadeIn,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }
    ).start()
    this.setState({ isHidden: true })
  }
  fadeOut() {
    Animated.timing(
      this.state.fadeOut,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }
    ).start()
    this.setState({ isHidden: false })
  }
  onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }


  // Sign in users with Auth
  async signIn() {

    const { username, password } = this.state
    this.setState({ load: true })
    await Auth.signIn(username, password)
      .then(user => {
        this.setState({ user })
        this.setState({ loginui: false, load: false })
        //this.props.navigation.navigate('Authloading')
      })
      .catch(err => {
        if (!err.message) {
          console.log('Error when signing in: ', err)

          Alert.alert(
            '',
            err,
            [
              //{text: 'OK', onPress: () => this.doRefreshState()},
              {
                text: 'OK', onPress: () => {

                  this.setState({ load: false });

                }
              }
            ],
            { cancelable: false }
          )

        } else {
          this.setState({ load: false })

          Alert.alert(
            '',
            err.message,
            [
              //{text: 'OK', onPress: () => this.doRefreshState()},
              {
                text: 'OK', onPress: () => {

                  this.setState({ load: false });

                }
              }
            ],
            { cancelable: false }
          )

        }
      })
  }


  StartImageRotateFunction() {

    this.RotateValueHolder.setValue(0)

    Animated.timing(
      this.RotateValueHolder,
      {
        toValue: 1,
        duration: 1000,
        delay: 1,
        easing: Easing.linear
      }
    ).start(() => this.StartImageRotateFunction())

  }


  // ConfirmSign in users with Auth
  async confirmSign() {

    this.setState({ load: true })
    await Auth.confirmSignIn(this.state.user, this.state.code, 'SMS_MFA')
      .then(user => {
        Alert.alert('Error when signing in: ', JSON.stringify(user))
        this.setState({ load: false })
      })
      .catch(err => {
        if (!err.message) {
          //    this.setState({load:false})
          Alert.alert(
            '',
            err,
            [
              //{text: 'OK', onPress: () => this.doRefreshState()},
              {
                text: 'OK', onPress: () => {

                  this.setState({ load: false });

                }
              }
            ],
            { cancelable: false }
          )

        } else {
          Alert.alert(
            '',
            err.message,
            [
              //{text: 'OK', onPress: () => this.doRefreshState()},
              {
                text: 'OK', onPress: () => {

                  this.setState({ load: false });

                }
              }
            ],
            { cancelable: false }
          )
        }
      })
  }



  ///SIgnup
  // Functions for Phone Input
  showModal() {
    this.bottomCountrySheet.showSizeSheet(
      data,
      this.state.phoneNumber
    );
    this.setState({ modalVisible: true })
    // console.log('Shown')
  }
  hideModal() {
    this.setState({ modalVisible: false })
    // refocus on phone Input after selecting country and/or closing Modal
    this.refs.FourthInput._root.focus()
    // console.log('Hidden')
  }
  async getCountry(country) {
    const countryData = await data
    try {
      const countryCode = await countryData.filter(
        obj => obj.name === country
      )[0].dial_code
      const countryFlag = await countryData.filter(
        obj => obj.name === country
      )[0].flag
      // Set data from user choice of country
      this.setState({ phoneNumber: countryCode, flag: countryFlag })
      await this.hideModal()
    }
    catch (err) {
      console.log(err)
    }
  }
  // Sign up user with AWS Amplify Auth
  async signUp() {
    this.setState({ load: true })
    const { username, password, email, phoneNumber } = this.state
    // rename variable to conform with Amplify Auth field phone attribute
    const phone_number = phoneNumber
    await Auth.signUp({
      username,
      password,
      attributes: { email, phone_number }
    })
      .then(() => {
        console.log('sign up successful!')
        this.setState({ loginui: false, load: false })
        //  Alert.alert('Enter the confirmation code you received.')
      })
      .catch(err => {
        if (!err.message) {
          this.setState({ load: false })
          console.log('Error when signing up: ', err)
          Alert.alert('Error when signing up: ', err)
        } else {
          this.setState({ load: false })
          console.log('Error when signing up: ', err.message)
          Alert.alert('Error when signing up: ', err.message)
        }
      })
  }
  // Confirm users and redirect them to the SignIn page
  async confirmSignUp() {
    this.setState({ load: true })

    const { username, authCode, email } = this.state
    await Auth.confirmSignUp(username, authCode)
      .then(() => {

        this.cllapiforemail(email)
        console.log(email)
        //this.props.navigation.navigate('SignInScreen')
        console.log('Confirm sign up successful')
      })
      .catch(err => {
        if (!err.message) {
          this.setState({ load: false })

          console.log('Error when entering confirmation code: ', err)
          Alert.alert('Error : ', err)
        } else {
          this.setState({ load: false })

          console.log('Error when entering confirmation code: ', err.message)
          Alert.alert('Error: ', err.message)
        }
      })
  }
  // email sent
  async cllapiforemail(email) {
    this.setState({ animating: true });
    console.log('call token');
    fetch('http://18.188.100.196:3000/send_welcome_email', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email
      }),
    })
      .then(response => response.json())
      .then((responseJson) => {
        console.log('end token');
        //alert(JSON.stringify(responseJson))
        this.props.navigation.navigate('SignInScreen')

        console.log(JSON.stringify(responseJson))
      })
      .catch((error) => {
        this.setState({ animating: false });
        //alert(JSON.stringify(error))
        this.props.navigation.navigate('SignInScreen')
      });

  }
  // Resend code if not received already
  async resendSignUp() {
    const { username } = this.state
    await Auth.resendSignUp(username)
      .then(() => console.log('Confirmation code resent successfully'))
      .catch(err => {
        if (!err.message) {
          console.log('Error requesting new confirmation code: ', err)
          Alert.alert('Error : ', err)
        } else {
          console.log('Error requesting new confirmation code: ', err.message)
          Alert.alert('Error : ', err.message)
        }
      })
  }

  selectedIetm = (item) =>{
this.getCountry(item.name)

  }


  render() {

    let { fadeOut, fadeIn, isHidden, flag } = this.state
    const countryData = data

    const RotateData = this.RotateValueHolder.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
    // this.StartImageRotateFunction();
    return (

      <SafeAreaView style={styles.container}>
        <OrientationLoadingOverlay
          visible={this.state.load}
        >
          <View>
            <Image
              source={require('../img/loadlogo.gif')}
              style={{ width: 80, height: 80 }} />
          </View>
        </OrientationLoadingOverlay>
        <StatusBar />

        <TouchableWithoutFeedback
          style={styles.container}
          onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={styles.container}
            behavior="padding">

            {this.state.loginui &&

              <ScrollView>
                <View style={styles.container}>
                  <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
                    <Image source={backarrow} style={{ width: 25, height: 25, margin: 10 }} />
                  </TouchableHighlight>

                  <View style={{ flexDirection: 'column' }}>
                    <Text style={{
                      fontSize: 22, margin: 10,
                      fontWeight: 'bold', color: '#000'
                    }}> SignUp to your Account </Text>

                  </View>
                  <Container style={styles.infoContainer}>

                    <View style={styles.container}>


                      <Text style={styles.itemText} >Username*</Text>

                      <Item style={styles.itemText}>
                        <Input
                          style={styles.input}
                          placeholder='Enter your uername'
                          placeholderTextColor='#adb4bc'
                          keyboardType={'email-address'}
                          returnKeyType='next'
                          autoCapitalize='none'
                          ref='FirstInput'
                          autoCorrect={false}
                          onSubmitEditing={(event) => { this.refs.FirstInput._root.focus() }}
                          onChangeText={value => this.onChangeText('username', value)}
                          onFocus={() => this.fadeOut()}
                          onEndEditing={() => this.fadeIn()}
                        />
                      </Item>



                      <Text style={styles.itemText}>Password*</Text>
                      <Item style={styles.itemText}>
                        <Input
                          style={styles.input}
                          placeholder='Enter your Password'
                          placeholderTextColor='#adb4bc'
                          returnKeyType='go'
                          autoCapitalize='none'
                          autoCorrect={false}
                          secureTextEntry={true}
                          onChangeText={value => this.onChangeText('password', value)}
                          onFocus={() => this.fadeOut()}
                          onEndEditing={() => this.fadeIn()}
                        />
                      </Item>

                      <Text style={styles.itemText} >Email*</Text>

                      <Item style={styles.itemText}>
                        <Input
                          style={styles.input}
                          placeholder='Enter your email'
                          placeholderTextColor='#adb4bc'
                          keyboardType={'email-address'}
                          returnKeyType='next'
                          autoCapitalize='none'
                          ref='FirstInput'
                          autoCorrect={false}
                          onSubmitEditing={(event) => { this.refs.FirstInput._root.focus() }}
                          onChangeText={value => this.onChangeText('email', value)}
                          onFocus={() => this.fadeOut()}
                          onEndEditing={() => this.fadeIn()}
                        />
                      </Item>





                      <Text style={styles.itemText}>Phone Number*</Text>
                      {/* phone section  */}
                      <Item style={styles.itemStyle}>

                        {/* country flag */}
                        <View><Text style={{ fontSize: 40, marginLeft: 10 }}>{flag}</Text></View>
                        {/* open modal */}
                        <TouchableOpacity onPress={() => this.showModal()}>
                          <Image
                            source={{ uri: 'https://cdn4.iconfinder.com/data/icons/ios-optimized-1/30/arrow__down__arrow-down__dropdown__download__chevron-down__set-512.png' }}

                            style={[styles.iconStyle, { marginLeft: 5 }]}

                          />
                        </TouchableOpacity>
                        <Input
                          style={styles.input}
                          placeholder='+1766554433'
                          placeholderTextColor='#adb4bc'
                          keyboardType={'phone-pad'}
                          returnKeyType='done'
                          autoCapitalize='none'
                          autoCorrect={false}
                          secureTextEntry={false}
                          ref='FourthInput'
                          value={this.state.phoneNumber}
                          onChangeText={(val) => {
                            if (this.state.phoneNumber === '') {
                              // render UK phone code by default when Modal is not open
                              this.onChangeText('phoneNumber', defaultCode + val)
                            } else {
                              // render country code based on users choice with Modal
                              this.onChangeText('phoneNumber', val)
                            }
                          }
                          }
                          onFocus={() => this.fadeOut()}
                          onEndEditing={() => this.fadeIn()}
                        />
                        
                       </Item>
                      



  


                      <TouchableOpacity
                        onPress={() => this.signUp()}
                        style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>
                          Sign Up
  </Text>

                      </TouchableOpacity>

                    </View>
                  </Container>
                  
                </View>
              </ScrollView>
            }

            {!this.state.loginui &&
              <View style={styles.container}>

                <TouchableHighlight
                  onPress={() => this.setState({ loginui: true })}>
                  <Image source={backarrow} style={{ width: 25, height: 25, marginLeft: 10, padding: 10 }} />
                </TouchableHighlight>

                <Container style={styles.infoContainer}>
                  <View style={styles.container}>

                    <View style={{ flexDirection: 'column' }}>
                      <Text style={{
                        fontSize: 22, margin: 10,
                        fontWeight: 'bold', color: 'black'
                      }}> Confirm Sign Up </Text>

                    </View>
                    <Text style={styles.itemText}>Confirmation Code*</Text>
                    <Item style={styles.itemStyle}>
                      <Input
                        style={styles.input}
                        placeholder='Enter your confirmation code'
                        placeholderTextColor='#adb4bc'
                        returnKeyType='go'
                        autoCapitalize='none'
                        autoCorrect={false}
                        secureTextEntry={false}
                        onChangeText={value => this.onChangeText('authCode', value)}
                        onFocus={() => this.fadeOut()}
                        onEndEditing={() => this.fadeIn()}
                      />
                    </Item>


                    <TouchableOpacity
                      onPress={() => this.confirmSignUp()}
                      style={styles.buttonStyle}>
                      <Text style={styles.buttonText}>
                        Confirm
                    </Text>
                    </TouchableOpacity>
                  </View>
                </Container>
              </View>
            }
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
        <BottomSheet
            ref={c => (this.bottomCountrySheet = c)}
            selectedIetm={this.selectedIetm}
            heading={"Select Country"}
          />

      </SafeAreaView>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    flexDirection: 'column'
  },
  input: {
    height: 60,
    borderWidth: 1,
    margin: 10,
    fontSize: 17,
    fontWeight: 'bold',
    color: '#000',
  },

  itemStyle: {
    marginBottom: 20,
  },
  itemText: {
    marginBottom: 3,
    marginStart: 14
  },
  iconStyle: {
    height: 30,
    width: 30,
    marginRight: 15
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#5372C4',
    padding: 14,
    margin: 14,
    marginBottom: 20,
    borderRadius: 3,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "#fff",
  },
  logoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 400,
    bottom: 180,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
})
