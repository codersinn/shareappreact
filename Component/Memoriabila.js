import React ,{Component} from 'react'
import {StyleSheet,Text,SafeAreaView,TouchableWithoutFeedback,View,Image,ImageBackground,TouchableHighlight} from 'react-native'

import logoImg from '../img/memorabilla.jpg';
import leftarrow from '../img/righticon.png';
import backarrow from '../img/backarrow.png';
import DetaillogoImg from '../img/memorabilla.jpg';

export default class Memoriabila extends Component{

  
    constructor(props){
        super(props)   
        //this.state ={texttitle : 'Comment on Facebook post' };    
    }
    render(){
        return(
          <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={styles.container}>
            <ImageBackground
                     
             source={logoImg} style={styles.image} >
             
             <View   style={{backgroundColor: 'rgba(52, 52, 52, 0.8)',width:'100%',flex:1,alignItems:'flex-start'}}>

             <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
<Image source={backarrow} style={{width:25,height:25,margin:10}} />
</TouchableHighlight>

<View style={{ flex: 1,
width:'100%',
    justifyContent: 'center',
    alignItems: 'center'}}>

<Text style={styles.centertext}>Memorabilia</Text>
</View>
             </View>


            
             </ImageBackground>

             <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CommentOnFacebookPost',{title:'Purchased an item',image:DetaillogoImg,points:'250',desc:'thank you for your purchase!  \n \n to recieve points,please press the \n button below'})}> 
     
  
            <View  style={styles.mainview}>
        <View style={{flex:1}} >
         
        <Text style={styles.title}>Purchased an item
</Text>
        <Text style={styles.titleshort}>250 Points</Text>
        </View>    
        <Image source={leftarrow} style={styles.shortimage} ></Image>        
       
      </View>

     </TouchableWithoutFeedback>
    

          </View>
         </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    mainview:{
        flexDirection: 'row',
        height: 40,
        marginTop:20,
        marginLeft:20,
        padding: 5,
        margin:2,
       
    },
    container:{
        backgroundColor:'#ffffff',
        flex:1,    
        alignItems:'center',
        justifyContent:'flex-start',
    },
    image: {
        width:'100%',
        height: 250,
        
      },
      shortimage: {
        width:15,
        marginTop:5,
        height: 15,
        marginLeft:5,
        alignItems: 'center'
      },
    title:{
        fontWeight:'bold',
        fontSize:14
    },
    titleshort:{
        color:'#B8B8B8',
        fontSize:10,

    },
    centertext:{
      color:'#ffffff',
      fontSize:20,
     
     
  }
})