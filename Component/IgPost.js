import React ,{Component} from 'react'
import {StyleSheet,Text,View,SafeAreaView,Image,ImageBackground,TouchableWithoutFeedback,TouchableHighlight} from 'react-native'

import logoImg from '../img/instagramselfie.jpg';
import leftarrow from '../img/righticon.png';
import backarrow from '../img/backarrow.png';
import RefeerallogoImg from '../img/instagramselfie.jpg';

export default class IgPost extends Component{

  
    constructor(props){
        super(props)       
    }
    render(){
        return(
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={styles.container}>
            <ImageBackground
                     
             source={logoImg} style={styles.image} >
             
             <View   style={{backgroundColor: 'rgba(52, 52, 52, 0.8)',width:'100%',flex:1,alignItems:'flex-start'}}>

             <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
<Image source={backarrow} style={{width:25,height:25,margin:10}} />
</TouchableHighlight>

<View style={{ flex: 1,
width:'100%',
    justifyContent: 'center',
    alignItems: 'center'}}>

<Text style={styles.centertext}>IG post</Text>
</View>
             </View>


            
             </ImageBackground>

             <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CommentOnFacebookPost',{title:'Selfie IG post',image:RefeerallogoImg,points:'25',desc:'did you happen to take a selfie with us  \n and post it on IG? \n \n thankyou so much to receive points.\n please press the button below'})}> 
 
      
            <View  style={styles.mainview}>
        <View style={{flex:1}} >
        <Text style={styles.title}>Selfie IG Post during a session

        </Text>
        <Text style={styles.titleshort}>25 points</Text>
        </View>    
        <Image source={leftarrow} style={styles.shortimage} ></Image>        
       
      </View>
      </TouchableWithoutFeedback>

      <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CommentOnFacebookPost',{title:'# us in a post',image:RefeerallogoImg,points:'5',desc:'did you just use our hashtags? \n  thank you so much! to receive points. \n \n please press the  button below'})}> 
 
      
 <View  style={styles.mainview}>
<View style={{flex:1}} >
<Text style={styles.title}># us in a post 

</Text>
<Text style={styles.titleshort}>5 points</Text>
</View>    
<Image source={leftarrow} style={styles.shortimage} ></Image>        

</View>
</TouchableWithoutFeedback>


     
         </View>
            </SafeAreaView>    
        )
    }
}

const styles = StyleSheet.create({

    mainview:{
        flexDirection: 'row',
        height: 40,
        marginTop:20,
        marginLeft:20,
        padding: 5,
        margin:2,
       
    },
    container:{
        backgroundColor:'#ffffff',
        flex:1,    
        alignItems:'center',
        justifyContent:'flex-start',
    },
    image: {
        width:'100%',
        height: 250,
        
      },
      shortimage: {
        width:15,
        marginTop:5,
        height: 15,
        marginLeft:5,
        alignItems: 'center'
      },
    title:{
        fontWeight:'bold',
        fontSize:14
    },
    titleshort:{
        color:'#B8B8B8',
        fontSize:10,

    },
    centertext:{
      color:'#ffffff',
      fontSize:20,
     
     
  }
})