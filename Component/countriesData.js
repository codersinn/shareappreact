const data = [

  {
    "name": "India",
    "flag": "🇮🇳",
    "code": "IN",
    "dial_code": "+91"
  },
  {
    "name": "United States",
    "flag": "🇺🇸",
    "code": "US",
    "dial_code": "+1"
  }
]

export default data