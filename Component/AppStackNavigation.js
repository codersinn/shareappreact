import {createStackNavigator, createAppContainer,createSwitchNavigator} from 'react-navigation';
import Details from './Details'
import DetailsSocial from './DetailsSocial'
import ActivtiyIndicator from './ActivityIndicatorExample'
import ReviewSubDetails from './ReviewSubDetails'
import ReviewOnYelp from './ReviewOnYelp'
import ReviewOnFacebook from './ReviewOnFacebook'


import LikeFacebookpage from './LikeFacebookpage'
import CommentOnFacebookPost from './CommentOnFacebookPost'
import ShareFaceBookPost from './ShareFaceBookPost'

import ReferalDetails from './ReferalDetails'
import ReferalAFeriend from './ReferalAFeriend'
import SignInScreen from './SignInScreen'

import SignUpScreen from './SignUpScreen'
import ForgotPassword from './ForgotPassword'
import AuthLoadingScreen from './AuthLoadingScreen'
import ProductPurchase from './ProductPurchase'
import ProfileScreen from './ProfileScreen'

 import ChangePassword from './ChangePassword'
import App from '../App'


const MainNavigator = createStackNavigator({
  
    App: {screen: App,    
        navigationOptions: {
          header: null,
        }      
    },

    ProductPurchase: {screen: ProductPurchase,    
      navigationOptions: {
        header: null,
      }      
  },
   
  ChangePassword: {screen: ChangePassword,    
    navigationOptions: {
      header: null,
    }      
},


    AuthLoadingScreen: {screen: AuthLoadingScreen,    
      navigationOptions: {
        header: null,
      }      
  },
  
    SignInScreen: {screen: SignInScreen,    
      navigationOptions: {
        header: null,
      }      
  },

  ForgotPassword: {screen: ForgotPassword,    
    navigationOptions: {
      header: null,
    }      
},


  SignUpScreen: {screen: SignUpScreen,    
    navigationOptions: {
      header: null,
    }      
},

    Details: {screen: Details, 
      navigationOptions: {
           header: null
   }},
   ReferalDetails: {screen: ReferalDetails, 
    navigationOptions: {
         header: null
 }},
    DetailsSocial : {
      screen:DetailsSocial,
      navigationOptions: {
        header: null
    }     
    },
    ReviewOnYelp : {
      screen:ReviewOnYelp,
      navigationOptions: {
        header: null
    }     
    },
    ReviewOnFacebook : {
      screen:ReviewOnFacebook,
      navigationOptions: {
        header: null
    }     
    },

    ReviewSubDetails:{

      screen:ReviewSubDetails,
      navigationOptions: {
        header: null
    } 
    },





    LikeFacebookpage : {
      screen:LikeFacebookpage,
      navigationOptions: {
        header: null
    }     
    },
    CommentOnFacebookPost : {
      screen:CommentOnFacebookPost,
      navigationOptions: {
        header: null
    }     
    },

    ShareFaceBookPost:{

      screen:ShareFaceBookPost,
      navigationOptions: {
        header: null
    } 
    },

    ReferalAFeriend:{

      screen:ReferalAFeriend,
      navigationOptions: {
        header: null
    } 
    },
    ActivtiyIndicator :{screen:ActivtiyIndicator,navigationOptions : {
      title: '',
     
      headerTitleStyle: { color: 'black' },
    }}
},
{
  // set default launcher  page
  //App
  //SignInScreen
  initialRouteName: "AuthLoadingScreen"
});

export default createAppContainer(MainNavigator);

