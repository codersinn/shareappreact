import React ,{Component} from 'react'
import {StyleSheet,Text,AsyncStorage,View,Image,Button,SafeAreaView,ImageBackground,TouchableOpacity,TouchableHighlight,Alert} from 'react-native'

import logoImg from '../img/socialmediacategory.jpg';
import leftarrow from '../img/righticon.png';
import backarrow from '../img/backarrow.png';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
var email = 'hi';
//var SharedPreferences = require('react-native-shared-preferences');


export default class CommentOnFacebookPost extends Component{

  
    constructor(props){
        super(props) 
        this.testVarible=this.props.navigation.state.name     
    }

    state = {
      animating: false
    }
    componentDidMount(){
      
          try {
            AsyncStorage.getItem('email', (err, result) => {
              if (result) {
                email = result.replace("\"", "").replace("\"", "").replace("","");
              console.log(email);
              }
              
             });
            // SharedPreferences.getItems(["email","event_id","phone_number","client_id"], function(values){
            //   email=values[0]
            
            //    });
      
         }
         catch (e) {
           //console.error(e.message);
         }
        
         
    
        }
    callapi(){
        this.setState({animating : true});
        console.log(email);
       
     fetch('http://18.188.100.196:3000/add_reward_data', {
       method: 'POST',
       headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
         email: email,
         type: this.props.navigation.getParam('title', 'NO-ID'),
         points: this.props.navigation.getParam('points', 'NO-ID'),
       }),
     })
     .then(response => response.json())
     .then((responseJson) => {

       this.setState ({animating : false});
       setTimeout(function(){
    
        //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
        alert(responseJson.message);
        //this.refs.toast.show(responseJson.message);
    
      }, 200);
       //alert(responseJson.message)
      })
     .catch((error) => {


      Alert.alert(
          
        err,
        [
          //{text: 'OK', onPress: () => this.doRefreshState()},
          { text: err, onPress: ()=>{

            this.setState({animating:false});

          } }
        ],
        { cancelable: false }
      )


       //this.setState ({animating : false});
       //alert(JSON.stringify(error))
     });;
    }
      
    render(){
        const { navigation } = this.props;
        //const{params} = navigation.state;
        const title = navigation.getParam('title', 'NO-ID');
         const image = navigation.getParam('image', 'som');
         const points = navigation.getParam('points', '25');
         const description = navigation.getParam('desc','desc');

    const name = navigation.getParam('name', 'NO-ID');
        return(

          
          <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={{flex:1}}>
               <OrientationLoadingOverlay
          visible={this.state.animating}  
          >
          <View>
          <Image  

    source={require('../img/loadlogo.gif')} 
    style={{width: 80, height: 80}}
    />
              </View>
          </OrientationLoadingOverlay>
            <View style={{flex:1}}>
            <ImageBackground
                     
             source={image} style={styles.image} >
             
             <View   style={{backgroundColor: 'rgba(52, 52, 52, 0.8)',width:'100%',flex:1,alignItems:'flex-start'}}>

             <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
<Image source={backarrow} style={{width:25,height:25,margin:10}} />
</TouchableHighlight>

<Text style={styles.centertext}>{title}
</Text>

             </View>


            
             </ImageBackground>
            <View  style={styles.mainview}>
            <View style={{ flex: 1,
    justifyContent: 'center',
    marginLeft:-15,
    alignItems: 'center'}}>
    

<Text style={styles.titlemargin}>{description}</Text>
        <Text style={styles.title}>Earn {points} Points</Text>
       
        </View>   

         
       
      </View>

      </View>


     
      {/* <Button title="Earn Points"
       buttonStyle={styles.button}
   onPress={()=>{this.callapi()}} /> */}

<TouchableOpacity
                    onPress={() => this.callapi()}
                    style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>
   EARN POINTS
  </Text>
           
                  </TouchableOpacity>  

          </View>
          </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    mainview:{
        flexDirection: 'row',
        height: 40,
        marginTop:20,
        marginLeft:20,
        padding: 5,
        margin:2,
       
    },

    buttonText: {
      fontSize: 18,
      fontWeight: 'bold',
      color: "#fff",
    },
     buttonStyle: {
      alignItems: 'center',
      backgroundColor: '#5372C4',
      padding: 10,
      margin:0,
      marginBottom: 5,
      borderRadius: 3,
    },
    button: {
      backgroundColor: '#5372C4',
      borderColor: '#5372C4',
      borderWidth: 5 ,
      height:80     
   },
    container:{
        backgroundColor:'#ffffff',
        flex:1,    
        alignItems:'center',
        justifyContent:'flex-start',
    },
    image: {
        width:'100%',
        height: 250,
        
      },
      shortimage: {
        width:15,
        marginTop:5,
        height: 15,
        marginLeft:5,
        alignItems: 'center'
      },
    title:{
        fontWeight:'bold',
        fontSize:20,
        height:60,
       color:'#5372C4',
       textAlign: 'center'
    },
    titlemargin:{
      fontWeight:'bold',
      fontSize:20,
      marginTop:200,
      height:120,
     color:'#5372C4',
     textAlign: 'center'
  },
    titleshort:{
        color:'#B8B8B8',
        fontSize:10,

    },
    centertext:{
      color:'#ffffff',
      fontSize:18,
      marginTop:'20%',
      textAlign:'center',
      flex:1,
      width:'100%'
      
     
  }
})