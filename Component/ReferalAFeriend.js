import React ,{Component} from 'react'
import {StyleSheet,Text,View,Image,Easing, Animated,Button,ImageBackground,TouchableHighlight} from 'react-native'

import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import loadingicon from '../img/loadinglogo.png';

import logoImg from '../img/referralscategory.jpg';
import leftarrow from '../img/righticon.png';
import backarrow from '../img/backarrow.png';

var email = 'hi';
var event_id = 'hi';

var phone_number = 'hi';

var client_id = 'hi';

export default class ReferalAFeriend extends Component{
   
  
    constructor(props){
        super(props) 
        this.state = { animating: false,text: 'Useless Placeholder'};
        this.RotateValueHolder = new Animated.Value(0);
       
        //this.testVarible = this.props.navigation.state.params.otherParam;
        //alert(testVarible);

    }

    componentDidMount(){
      
    //   try {
    //     SharedPreferences.getItems(["email","event_id","phone_number","client_id"], function(values){
    //       email=values[0]
    //       event_id=values[1]
    //       phone_number=values[2]
    //       client_id=values[3]
    //        });
  
    //  }
    //  catch (e) {
    //    console.error(e.message);
    //  }
    
     

    }

  onclick() {
    // this.setState({
    //   animating: true })
      this.callapi()
  
  }
 
 StartImageRotateFunction () {
 
  this.RotateValueHolder.setValue(0)
  
  Animated.timing(
    this.RotateValueHolder,
    {
      toValue: 1,
      duration: 7000,
      delay:10,
      easing: Easing.linear
    }
  ).start(() => this.StartImageRotateFunction())
 
 }
 
 callapi(){
     this.setState({animating : true});
    
  fetch('http://18.188.100.196:3000/add_reward_data', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: email,
      type: 'refer friend',
      points: '500',
    }),
  })
  .then(response => response.json())
  .then((responseJson) => {
    this.setState ({animating : false});
    alert(JSON.stringify(responseJson))
  })
  .catch((error) => {
    this.setState ({animating : false});
    alert(JSON.stringify(error))
  });;
 }
   
    render(){
        const { navigation } = this.props;
    // const itemId = navigation.getParam('itemId', 'NO-ID');
    // const otherParam = navigation.getParam('otherParam', 'some default');
    const RotateData = this.RotateValueHolder.interpolate({
       inputRange: [0, 1],
       outputRange: ['0deg', '360deg']
     })
        return(
            <View style={{flex:1}}>
            <View style ={{flex:1}}>

            <View style={{flex:1}}>
            <ImageBackground
                     
             source={logoImg} style={styles.image} >
             
             <View   style={{backgroundColor: 'rgba(52, 52, 52, 0.8)',width:'100%',flex:1,alignItems:'flex-start'}}>

             <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
<Image source={backarrow} style={{width:25,height:25,margin:10}} />
</TouchableHighlight>

<Text style={styles.centertext}> Refer a friend


</Text>

             </View>
            
             </ImageBackground>
            <View  style={styles.mainview}>
            <View style={{ flex: 1,
    justifyContent: 'center',
    alignItems: 'center'}}>

     
        <Text style={styles.title}>Earn 1500 Points</Text>
     
        </View>          
       
      </View>
 
      </View>
     
      <Button title="Earn Reward"
  color="#5372C4" style={{height:80}}  onPress={()=>{this.onclick()}}/>
  </View>
  <OrientationLoadingOverlay
          visible={this.state.animating}

          >
          <View>
          <Image  

    source={require('../img/loadlogo.gif')} 
    style={{width: 80, height: 80}} />
              </View>
          </OrientationLoadingOverlay>
          </View>
        )
    }
}

const styles = StyleSheet.create({

    mainview:{
        flexDirection: 'row',
        height: 40,
        marginTop:20,
        marginLeft:20,
        padding: 5,
        margin:2,
       
    },
    container:{
        backgroundColor:'#ffffff',
        flex:1,    
        alignItems:'center',
        justifyContent:'flex-start',
    },
    image: {
        width:'100%',
        height: 250,
        
      },
      shortimage: {
        width:15,
        marginTop:5,
        height: 15,
        marginLeft:5,
        alignItems: 'center'
      },
    title:{
        fontWeight:'bold',
        fontSize:20,
       color:'#5372C4'
    },
    titleshort:{
        color:'#B8B8B8',
        fontSize:10,

    },
    centertext:{
      color:'#ffffff',
      fontSize:20,
      marginLeft:'30%',
      marginTop:'20%'
     
  }
})