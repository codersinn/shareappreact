import React ,{Component} from 'react'
import {StyleSheet,Text,View,Image,Platform,BackHandler,Button,SafeAreaView,ImageBackground,TouchableHighlight,TouchableOpacity,AsyncStorage} from 'react-native'
//var SharedPreferences = require('react-native-shared-preferences');

import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import loadingicon from '../img/loadinglogo.png';
import logoImg from '../img/referralscategory.jpg';
import leftarrow from '../img/righticon.png';
import backarrow from '../img/backarrow.png';
import Toast, {DURATION} from 'react-native-easy-toast';

var email = 'hi';
var event_id = 'hi';

var phone_number = 'hi';

var client_id = 'hi';

export default class ProductPurchase extends Component{
   
  
    constructor(props){
        super(props) 
        this.state = { animating: false,text: 'Useless Placeholder'};
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        //this.toast = null;

    }

    componentDidMount()
    {                 
      try {
        AsyncStorage.getItem('email', (err, result) => {
          email = result;
          //alert(email);
          console.log(email);
         });
        // SharedPreferences.getItems(["email","event_id","phone_number","client_id"], function(values){
        //   email=values[0]
        
        //    });
  
     }catch (e) {
      console.error(e.message);
    }
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick() {
      //alert('hello');
      this.props.navigation.goBack(null);
      return true;
    }
  
  onclick(point,productname,productid) {
    //alert(point+productname+productid);
    this.setState({animating : true});
   
 fetch('http://18.188.100.196:3000/redeem_reward', {
   method: 'POST',
   headers: {
     Accept: 'application/json',
     'Content-Type': 'application/json',
   },
   body: JSON.stringify({
     email: email,
     product_id: productid,
     product_name: productname,
     product_point: point,
   }),
 })
 .then(response => response.json())
 .then((responseJson) => {
   this.setState ({animating : false});
   console.log(responseJson.message);
   
   setTimeout(function(){
    
    //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
    alert(responseJson.message);
    //this.refs.toast.show(responseJson.message);

  }, 200);
  
   //setTimeout(() => alert(responseJson.message), 4000);
   //alert(responseJson.message)
 })
 .catch((error) => {
  

   this.setState ({animating : false});
   
   setTimeout(function(){
    
    //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
    alert(error);
    //this.refs.toast.show(responseJson.message);

  }, 200);
  // alert(JSON.stringify(error))
  
 });;
 
  }
 

    render(){
        const { navigation } = this.props;
    const itemId = navigation.getParam('tit', 'T-shir');
    const price = navigation.getParam('pric', '1');
    const image = navigation.getParam('imageurl','')
    const coin  = navigation.getParam('coi','1')

    //alert(coin+itemId+image);

    
    var randomImages = [
     
      require('../img/freetshirt.jpg'),
      require('../img/yogamat.jpg'),
      require('../img/tendiscount.jpg'),
      require('../img/freepersonalt.jpg'),
      require('../img/freemealplan.jpg'),
      require('../img/tendiscount.jpg')

  ];
    
        return(

          <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={{flex:1}}>

            

            <View style={{flex:1}}>
            <View
                     
             style={styles.image} >
             <Image  
                     source={randomImages[parseInt(image)]}
                     style={styles.image} />
                  
             <View   style={{backgroundColor: 'rgba(52, 52, 52, 0.8)',width:'100%',flex:1,alignItems:'flex-start',zIndex:2}}>

             <TouchableHighlight onPress={() => this.props.navigation.goBack()}>
<Image source={backarrow} style={{width:25,height:25,margin:10}} />
</TouchableHighlight>



<Text style={{width:'100%',height:45,marginTop:'15%',textAlign:'center',color:'white',fontSize:20}} > {itemId}

</Text>

             </View>
            
             </View>
            
            <View style={{ flex: 1,
                  justifyContent: 'flex-start',
                  marginTop:240,
                  padding:10,
                  backgroundColor:'#fff',
                  alignItems: 'center'}}>

       
        {/* <Text style={styles.title}>Spent {coin} Points  {price}  </Text> */}
        <Text style={styles.title}> looks like someone is using their {'\n'} points! </Text>
        <Text style={styles.title}> to confirm, please press the {'\n'} button below </Text>

       
      
        </View>          
      
          
        <Toast
                    ref="toast"
                    style={{backgroundColor:'#5372C4'}}
                    position='center'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.9}
                    textStyle={{color:'white'}}
                />
     
      {/* <Button title="Buy Now"       
       buttonStyle={styles.button}
  onPress={()=>{this.onclick(coin,itemId,image)}}/> */}
   <TouchableOpacity
                    onPress={() => this.onclick(coin,itemId,image)}
                    style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>
    BUY NOW
  </Text>
           
                  </TouchableOpacity>    

  </View>
              
  <OrientationLoadingOverlay
          visible={this.state.animating}
          >
          <View>
          <Image  
          style={{width: 80, height: 80}}
    source={require('../img/loadlogo.gif')} />
              </View>
          </OrientationLoadingOverlay>
          </View>
          </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    mainview:{
        flexDirection: 'row',
        height: 40,
        marginTop:20,
        marginLeft:20,
        padding: 5,
        margin:2,
       
    },
    button: {
      backgroundColor: '#5372C4',
      borderColor: '#5372C4',
      borderWidth: 5 ,
      height:80,
      marginBottom:10,
      width:'100%',
    
   },
   buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "#fff",
  },
   buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#5372C4',
    padding: 10,
    margin:0,
    marginBottom: 5,
    borderRadius: 3,
  },
    container:{
        backgroundColor:'#ffffff',
        flex:1,    
        alignItems:'center',
        justifyContent:'flex-start',
    },
    image: {
        width:'100%',
        height: 250,
        position:'absolute'                
      },
      shortimage: {
        width:15,
        marginTop:5,
        height: 15,
        marginLeft:5,
        alignItems: 'center'
      },
    title:{
        fontWeight:'bold',
        fontSize:20,
        marginTop:50,
        textAlign:'center',
       color:'#5372C4'
    },
    titleshort:{
        color:'#B8B8B8',
        fontSize:10,

    },
    centertext:{
      color:'#ffffff',
      fontSize:20,
      marginLeft:'25%',
      marginTop:'20%'
     
  }
})