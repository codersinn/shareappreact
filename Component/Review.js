
    
import React, { Component } from 'react';
import { StyleSheet,Image,ImageBackground, Text, View ,TouchableOpacity} from 'react-native';
import { Container,Grid,Row,Col, Header,Card,CardItem, Content, Footer, FooterTab, Button, Icon } from 'native-base';

import Tabbar from 'react-native-tabbar-bottom'
import {Actions} from 'react-native-router-flux';

import bgSrc1 from '../img/blur_background.jpg';
import bgSrc2 from '../img/bottel.jpg';
import bgSrc3 from '../img/hangbottel.jpg';
import bgSrc4 from '../img/images.jpg';

export default class Review extends Component {
    constructor() {
        super()
        this.state = {
          page: "HomeScreen",
        }
      }
	signup() {
		Actions.signup()
	}

    render() {
        return (
        
          <View style={styles.container}>
          {
           
            // if you are using react-navigation just pass the navigation object in your components like this:
            // {this.state.page === "HomeScreen" && <MyComp navigation={this.props.navigation}>Screen1</MyComp>}
          }
       
          {this.state.page === "HomeScreen" && 
          <Container>
            <Header style={{ backgroundColor: 'white',alignItems:'center',
            justifyContent:'center'}}><Text style={{ Color: 'lightgray',fontSize:18}}>Earn Rewards  </Text>
            

    
            </Header>
              <Grid>
             
                <Col onPress={this.signup} style={{padding :1,paddingLeft:0}}>
                <ImageBackground  source={bgSrc1} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}> 
             <Text style={{ color: 'white'}}>Reviews</Text>
                </ImageBackground>
                </Col>
               
               
                <Col style={{padding :1,paddingRight:0}}>
                <ImageBackground  source={bgSrc2} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
            <Text style={{ color: 'white'}}> Comment on website blog </Text>
           
                </ImageBackground>
                </Col>
               
              </Grid>
              
              <Grid>
                
                <Col style={{padding :1,paddingLeft:0}}>
                <ImageBackground  source={bgSrc1} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
             <Text style={{ color: 'white'}}>Follow on Instagram</Text>
               </ImageBackground>
                 </Col>
             
                <Col style={{padding :1,paddingLeft:0}}>
                <ImageBackground  source={bgSrc1} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
             <Text style={{ color: 'white'}}>Share on Facebook </Text>
                </ImageBackground>
                </Col>
              
               
               </Grid>
               
              
              <Grid>
                
                <Col style={{padding :1,paddingLeft:0}}>
                <ImageBackground  source={bgSrc1} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
             <Text style={{ color: 'white'}}>Guest Comment/Blogging
     </Text>
                </ImageBackground>
                </Col>
              
               
                <Col style={{padding :1,paddingRight:0}}>
                <ImageBackground  source={bgSrc2} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
            <Text style={{ color: 'white'}}> Comment on Insta </Text>
           
                </ImageBackground>
                </Col>
               
              </Grid>
        </Container>}
          {this.state.page === "NotificationScreen" && <Text>Screen2</Text>}
          {this.state.page === "ProfileScreen" &&  <Container>
          <Header style={{ backgroundColor: 'white',alignItems:'center',
            justifyContent:'center'}}><Text style={{ Color: 'lightgray',fontSize:18}}>SHOP </Text></Header>
              <Grid>
                
                <Col style={{padding :1,paddingLeft:0}}>
                <ImageBackground  source={bgSrc1} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
             <Text style={{ color: 'white'}}>T- Shirt </Text>
            <Text style={{ color: 'white'}}> 200 Reward Points  </Text>
                <Button success style={{ alignItems:'center',
            justifyContent:'center'}}><Text> Shop Now </Text></Button>
                </ImageBackground>
                </Col>
              
               
                <Col style={{padding :1,paddingRight:0}}>
                <ImageBackground  source={bgSrc4} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
            <Text style={{ color: 'white'}}> Hoodie </Text>
            <Text style={{ color: 'white'}}> 350 Reward Points </Text>
                <Button success><Text> Shop Now </Text></Button>
                </ImageBackground>
                </Col>
               
              </Grid>
              <Grid>
                <Col style={{padding :1,paddingLeft:0}}>
                <ImageBackground  source={bgSrc3} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
             <Text style={{ color: 'white'}}> Gym Bag </Text>
            <Text style={{ color: 'white'}}> 250 Reward Points </Text>
                <Button success><Text> Shop Now </Text></Button>
                </ImageBackground>
                </Col>
                <Col style={{padding :1,paddingRight:0}}>
               
                <ImageBackground  source={bgSrc2} style={{flex: 1, resizeMode: 'cover',alignItems:'center',
            justifyContent:'center'}}>
             <Text style={{ color: 'white'}}> Sipper </Text>
            <Text style={{ color: 'white'}}> 150 Reward Points </Text>
                <Button success style={{ }}><Text> Shop Now </Text></Button>
                </ImageBackground>
                </Col>
              </Grid>
             
             
               
                
              
          </Container>}
          {this.state.page === "ChatScreen" && <Text>Screen4</Text>}
          {this.state.page === "SearchScreen" && <Text>Screen5</Text>}
         
    <Footer>
          <Tabbar
            stateFunc={(tab) => {
              this.setState({page: tab.page})
              //this.props.navigation.setParams({tabTitle: tab.title})
            }}
            activePage={this.state.page}
            tabs={[
              {
                page: "HomeScreen",
                icon: "home",
              },
              {
                page: "NotificationScreen",
                icon: "person"
              },
              {
                page: "ProfileScreen",
                icon: "person",
              },
              {
                page: "ChatScreen",
                icon: "notifications",
              },
              {
                page: "SearchScreen",
                icon: "person",
              },
            ]}
          />
          </Footer>
        </View>
        );
      }
}
const styles = StyleSheet.create({
  container : {
    backgroundColor:'#455a64',
    flex: 1,
  
  },
  signupTextCont : {
  	flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:16,
    flexDirection:'row'
  },
  signupText: {
  	color:'rgba(255,255,255,0.6)',
  	fontSize:16
  },
  signupButton: {
  	color:'#ffffff',
  	fontSize:16,
  	fontWeight:'500'
  }
});
