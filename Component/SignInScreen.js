import React from 'react'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import backarrow from '../img/blackback_arrow.png';
//var SharedPreferences = require('react-native-shared-preferences');
import Toast, { DURATION } from 'react-native-easy-toast';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
  Easing,
  AsyncStorage,
  Image,
  Platform,
  TouchableHighlight,
  Button,
  BackHandler,
  StatusBar,
  KeyboardAvoidingView,
  Keyboard,
  Alert,
  Animated,
} from 'react-native'


import {
  Container,
  Item,
  Input
} from 'native-base'
//import FCM, { NotificationActionType } from "react-native-fcm";
// AWS Amplify modular import
//import Auth from '@aws-amplify/auth'


import awsConfig from '../Src/aws-exports';

import Amplify, { Auth } from 'aws-amplify';

var token = 'demo token right now';

// Load the app logo
Amplify.configure(awsConfig);

export default class SignInScreen extends React.Component {


  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

  }
  state = {
    username: '',
    password: '',
    code: '',
    fadeIn: new Animated.Value(0),
    fadeOut: new Animated.Value(0),
    isHidden: false,
    loginui: true,
    load: false
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    //alert('hello');
    BackHandler.exitApp();
    //this.props.navigation.goBack(null);
    return true;
  }


  componentDidMount() {

    //   FCM.getFCMToken().then(device_token => {
    //    // alert(device_token);
    //     token = device_token;
    //      // Send token to your server here.
    //      this.storeItem("token",token)
    //    });

    //    if (Platform.OS === "ios") {
    //      FCM.getAPNSToken().then(token => {
    //        console.log("APNS TOKEN (getFCMToken)", token);
    //        this.storeItem("apnsToken",token)

    //      });
    //  }
    this.fadeIn()
  }
  fadeIn() {
    Animated.timing(
      this.state.fadeIn,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }
    ).start()
    this.setState({ isHidden: true })
  }
  fadeOut() {
    Animated.timing(
      this.state.fadeOut,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }
    ).start()
    this.setState({ isHidden: false })
  }
  onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }


  // Sign in users with Auth
  async signIn() {
    const { username, password } = this.state
    this.setState({ load: true })
    await Auth.signIn(username, password)
      .then(user => {

        this.setState({ user })
        //Alert.alert('Error when signing in: ', 'mohit');
        //this.refs.toast.show('code sent to registered mobile number');
        this.setState({ loginui: false, load: false })


      })
      .catch(err => {

        if (!err.message) {


          this.setState({ load: false });


          setTimeout(function () {

            alert(err.message);

          }, 200);


        } else {

          this.setState({ load: false });


          setTimeout(function () {

            alert(err.message);

          }, 200);
        }
      })
  }

  async cllapiforSendToken(username, email, token) {
    this.setState({ animating: true });
    console.log('call token');
    fetch('http://18.188.100.196:3000/save_user_data', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        email: email,
        device_type: Platform.OS,
        device_token: token
      }),
    })
      .then(response => response.json())
      .then((responseJson) => {
        console.log('end token');
        //alert(JSON.stringify(responseJson))
        this.setState({ animating: false });
        this.props.navigation.navigate('App');

        console.log(JSON.stringify(responseJson))
      })
      .catch((error) => {

        this.setState({ animating: false });
        this.props.navigation.navigate('App');
        // alert(JSON.stringify(error))
      });

  }

  async storeItem(key, item) {
    try {
      //we want to wait for the Promise returned by AsyncStorage.setItem()
      //to be resolved to the actual value before returning the value
      await AsyncStorage.setItem(key, item);

    } catch (error) {
      console.log(error.message);
    }
  }

  // ConfirmSign in users with Auth
  async confirmSign() {
    console.log('con');
    this.setState({ load: true })
    await Auth.confirmSignIn(this.state.user, this.state.code, 'SMS_MFA')
      .then(user => {

        this.storeItem("username", user.username)
        console.log(user.username);
        this.storeItem("email", user.signInUserSession.idToken.payload.email)
        console.log(user.signInUserSession.idToken.payload.email);
        //this.storeItem("key","value")
        this.storeItem("event_id", user.signInUserSession.idToken.payload.event_id)
        console.log(user.signInUserSession.idToken.payload.event_id);
        this.storeItem("phone_number", user.signInUserSession.idToken.payload.phone_number)

        this.storeItem("client_id", user.signInUserSession.accessToken.payload.client_id)




        this.setState({ load: false });
        console.log('con end');
        this.props.navigation.navigate('App');
        //  this.cllapiforSendToken(user.username,user.signInUserSession.idToken.payload.email,token);

      })
      .catch(err => {
        if (!err.message) {
          this.setState({ load: false });


          setTimeout(function () {

            alert(err.message);

          }, 200);

        } else {

          this.setState({ load: false });


          setTimeout(function () {

            alert(err.message);

          }, 200);

        }
      })
  }


  render() {

    let { fadeOut, fadeIn, isHidden } = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <OrientationLoadingOverlay
          visible={this.state.load}
        >
          <View>
            <Image

              source={require('../img/loadlogo.gif')}
              style={{ width: 80, height: 80 }}
            />
            <Toast
              ref="toast"
              style={{ backgroundColor: '#5372C4' }}
              position='center'
              positionValue={200}
              fadeInDuration={750}
              fadeOutDuration={2000}
              opacity={0.9}
              textStyle={{ color: 'white' }}
            />
          </View>
        </OrientationLoadingOverlay>
        <StatusBar />

        <TouchableWithoutFeedback
          style={styles.container}
          onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={styles.container}
            behavior="padding">

            {this.state.loginui &&


              <View style={styles.container}>

                <View style={{ flexDirection: 'column' }}>
                  <Text style={{
                    fontSize: 22, margin: 10,
                    fontWeight: 'bold', color: '#000'
                  }}> Sign In to your Account </Text>

                </View>
                <Container style={styles.infoContainer}>

                  <View style={styles.container}>


                    <Text style={styles.itemText} >Username*</Text>

                    <Item style={styles.itemText}>
                      <Input
                        style={styles.input}
                        placeholder='Enter your uername'
                        placeholderTextColor='#adb4bc'
                        keyboardType={'email-address'}
                        returnKeyType='next'
                        autoCapitalize='none'
                        ref='FirstInput'
                        autoCorrect={false}
                        onSubmitEditing={(event) => { this.refs.FirstInput._root.focus() }}
                        onChangeText={value => this.onChangeText('username', value)}
                        onFocus={() => this.fadeOut()}
                        onEndEditing={() => this.fadeIn()}
                      />
                    </Item>



                    <Text style={styles.itemText}>Password*</Text>
                    <Item style={styles.itemText}>
                      <Input
                        style={styles.input}
                        placeholder='Enter your Password'
                        placeholderTextColor='#adb4bc'
                        returnKeyType='go'
                        autoCapitalize='none'
                        autoCorrect={false}
                        secureTextEntry={true}
                        onChangeText={value => this.onChangeText('password', value)}
                        onFocus={() => this.fadeOut()}
                        onEndEditing={() => this.fadeIn()}
                      />
                    </Item>
                    <TouchableOpacity
                      onPress={() => this.signIn()}
                      style={styles.buttonStyle}>
                      <Text style={styles.buttonText}>
                        Sign In
  </Text>

                    </TouchableOpacity>


                    <View style={{
                      flexDirection: 'row', flex: 1,
                      paddingLeft:30,
                      paddingTop:30
                    }}>

                      <View style={styles.buttonContainer}>
                        <TouchableOpacity
                          onPress={() => this.props.navigation.navigate('SignUpScreen')
                          }
                          >
                          <Text style={{color:'#5372C4'}} >
                            Sign Up
                  </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.buttonContainer}>
                        <TouchableOpacity
                          onPress={() => this.props.navigation.navigate('ForgotPassword')
                          }
                          >
                          <Text style={{color:'#5372C4', paddingLeft:30,}}>
                            Forgot Password
                  </Text>
                        </TouchableOpacity>
                      </View>




                    </View>
                  </View>
                </Container>
              </View>
            }

            {!this.state.loginui &&
              <View style={styles.container}>
                <TouchableHighlight
                  onPress={() => this.setState({ loginui: true })}>
                  <Image source={backarrow} style={{ width: 25, height: 25, marginLeft: 10 }} />
                </TouchableHighlight>


                <Container style={styles.infoContainer}>
                  <View style={styles.container}>

                    <View style={{ flexDirection: 'column' }}>
                      <Text style={{
                        fontSize: 22, margin: 10,
                        fontWeight: 'bold', color: 'black'
                      }}> Confirm Sign In </Text>

                    </View>
                    <Text style={styles.itemText}>Confirmation Code*</Text>
                    <Item style={styles.itemStyle}>
                      <Input
                        style={styles.input}
                        placeholder='Enter your confirmation code'
                        placeholderTextColor='#adb4bc'
                        returnKeyType='go'
                        autoCapitalize='none'
                        autoCorrect={false}
                        secureTextEntry={false}
                        onChangeText={value => this.onChangeText('code', value)}
                        onFocus={() => this.fadeOut()}
                        onEndEditing={() => this.fadeIn()}
                      />
                    </Item>


                    <TouchableOpacity
                      onPress={() => this.confirmSign()}
                      style={styles.buttonStyle}>
                      <Text style={styles.buttonText}>
                        Confirm
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this.setState({ loginui: true })}
                      style={{
                        backgroundColor: '#fff'
                      }}
                    >
                      <Text style={styles.buttonStyleLikeText}>

                        Sign In
                    </Text>
                    </TouchableOpacity>


                  </View>
                </Container>
              </View>
            }
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>

      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    flexDirection: 'column'
  },
  input: {
    height: 60,
    borderWidth: 1,
    margin: 10,
    fontSize: 17,
    fontWeight: 'bold',
    color: '#000',
  },
  buttonContainer: {
    flex: 1,
  },
  itemStyle: {
    marginBottom: 20,
  },
  itemText: {
    marginBottom: 3,
    marginStart: 14
  },
  iconStyle: {
    color: '#fff',
    fontSize: 30,
    marginRight: 15
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#5372C4',
    padding: 14,
    margin: 14,
    marginBottom: 20,
    borderRadius: 3,
  },
  buttonStyleLikeText: {
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#fff',
    padding: 14,
    height: 175,
    margin: 14,
    color: '#5372C4'
  },

  buttonStyleLikeMargin: {
    alignItems: 'center',
    width: '50%',
    marginStart: 20,
    backgroundColor: '#fff',
    padding: 14,
    height: 55,
    margin: 14,
    color: '#5372C4'
  },

  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "#fff",
  },
  logoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 400,
    bottom: 180,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
})
