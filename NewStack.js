//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { View, Image, TouchableOpacity,Text } from 'react-native';
// import all basic components
//For React Navigation 2.+ import following
//import {DrawerNavigator, StackNavigator} from 'react-navigation';
//For React Navigation 3.+ import following
import {
  createDrawerNavigator,
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator
} from 'react-navigation';
 


import Details from './Component/Details'
import DetailsSocial from './Component/DetailsSocial'
import ActivtiyIndicator from './Component/ActivityIndicatorExample'
import ReviewSubDetails from './Component/ReviewSubDetails'
import ReviewOnYelp from './Component/ReviewOnYelp'
import ReviewOnFacebook from './Component/ReviewOnFacebook'
import FacebookCheckIn from'./Component/FacebookCheckIn'


import LikeFacebookpage from './Component/LikeFacebookpage'
import CommentOnFacebookPost from './Component/CommentOnFacebookPost'
import ShareFaceBookPost from './Component/ShareFaceBookPost'

import ReferalDetails from './Component/ReferalDetails'
import ReferalAFeriend from './Component/ReferalAFeriend'
import SignInScreen from './Component/SignInScreen'

import SignUpScreen from './Component/SignUpScreen'
import ForgotPassword from './Component/ForgotPassword'
import AuthLoadingScreen from './Component/AuthLoadingScreen'
import ProductPurchase from './Component/ProductPurchase'

import ProfileScreen from './Component/ProfileScreen'
import IgPost from './Component/IgPost'
import Memoriabila from './Component/Memoriabila'



 import ChangePassword from './Component/ChangePassword'

import Notification from './Component/Notification'
import {AppStackNavigation} from './Component/AppStackNavigation'
 import App from './App'
 import Category from './Category'
 import SubCategory from './SubCategory'

 
 


class NavigationDrawerStructure extends Component {
  //Structure for the navigatin Drawer
  toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };
  render() {
    return (
      <View style={{ flexDirection: 'row',height:0}}>
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
          {/*Donute Button Image */}
          <Text
            style={{ width: 45, height: 25, marginLeft: 15,color:'red' }}
          >drawer
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
 
//For React Navigation 2.+ need to use StackNavigator instead createStackNavigator
//const FirstActivity_StackNavigator = StackNavigator({
//For React Navigation 3.+

 
//For React Navigation 2.+ need to use StackNavigator instead createStackNavigator
//const FirstActivity_StackNavigator = StackNavigator({
//For React Navigation 3.+

 
//For React Navigation 2.+ need to use StackNavigator instead createStackNavigator
//const FirstActivity_StackNavigator = StackNavigator({
//For React Navigation 3.+

 
//For React Navigation 2.+ need to use DrawerNavigator instead createDrawerNavigator
//const DrawerNavigatorExample = DrawerNavigator({
//For React Navigation 3.+

const MainNavigator = createStackNavigator({
  
  Category: {
    screen: Category,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,

    } 
  },

  SubCategory: {
    screen: SubCategory,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
      transition:null
    } 
  },
  
  ChangePassword: {screen: ChangePassword,    
    navigationOptions: {
      header: null,
    }      
},
    Memoriabila: {screen: Memoriabila,    
      navigationOptions: {
        header: null,
      }      
  },
  ProfileScreen: {screen: App,    
    navigationOptions: {
      header: null,
    }      
},

    ProductPurchase: {screen: ProductPurchase,    
      navigationOptions: {
        header: null,
      }      
  },
  IgPost: {screen: IgPost,    
    navigationOptions: {
      header: null,
    }      
},
FacebookCheckIn: {screen: FacebookCheckIn,    
  navigationOptions: {
    header: null,
  }      
},


    AuthLoadingScreen: {screen: AuthLoadingScreen,    
      navigationOptions: {
        header: null,
      }      
  },
  
    SignInScreen: {screen: SignInScreen,    
      navigationOptions: {
        header: null,
      }      
  },

  ForgotPassword: {screen: ForgotPassword,    
    navigationOptions: {
      header: null,
    }      
},


  SignUpScreen: {screen: SignUpScreen,    
    navigationOptions: {
      header: null,
    }      
},

    Details: {screen: Details, 
      defaultNavigationOptions: {
        gesturesEnabled: false
    },
      navigationOptions: {
           header: null
   }},
   ReferalDetails: {screen: ReferalDetails, 
    navigationOptions: {
         header: null
 }},
    DetailsSocial : {
      screen:DetailsSocial,
      navigationOptions: {
        header: null
    }     
    },
    ReviewOnYelp : {
      screen:ReviewOnYelp,
      navigationOptions: {
        header: null
    }     
    },
    ReviewOnFacebook : {
      screen:ReviewOnFacebook,
      navigationOptions: {
        header: null
    }     
    },

    ReviewSubDetails:{

      screen:ReviewSubDetails,
      navigationOptions: {
        header: null
    } 
    },
    LikeFacebookpage : {
      screen:LikeFacebookpage,
      navigationOptions: {
        header: null
    }     
    },
    CommentOnFacebookPost : {
      screen:CommentOnFacebookPost,
      navigationOptions: {
        header: null
    }     
    },

    ShareFaceBookPost:{

      screen:ShareFaceBookPost,
      navigationOptions: {
        header: null
    } 
    },

    ReferalAFeriend:{

      screen:ReferalAFeriend,
      navigationOptions: {
        header: null
    } 
    },
    ActivtiyIndicator :{screen:ActivtiyIndicator,navigationOptions : {
      title: '',     
      headerTitleStyle: { color: 'black' },
    }
  },
  
 
  App: {
    screen: App,
    navigationOptions: {
      header: null,
    } 
  },

  ChangePassword: {screen: ChangePassword,    
    navigationOptions: {
      header: null,
    }      
},

  Notification: {
    //Title
    screen: Notification,
    navigationOptions: {
     drawerLabel : 'Notification'
    },
  },
    
    

});









 
//For React Navigation 2.+ need to export App only
//export default App;
//For React Navigation 3.+
export default createAppContainer(MainNavigator);
