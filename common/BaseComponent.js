import React, { Component } from 'react';
import {  Platform, BackHandler ,Alert} from 'react-native';
class Basecomponents extends Component {

  static navigationOptions = ({ navigation }) => ({
    header: null,
    gesturesEnabled: false

  });
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  

  handleBackButtonClick = () => {
    this.props.navigation.goBack()
    return true;
  }

 
  // strings(name, params = {}) {
  //   return strings(name, params);
  // };

  getDeviceType = () => {

    return Platform.OS;
  }
  

  showToast = (message) => {
   // setTimeout(() => { Toast.show(message, Toast.CENTER) }, 200)
  }

  openDrawer = () => {
    this.props.navigation.openDrawer();
  }

  
}


export function showMessage(isError=false,message){
  Alert.alert(
    isError ? Error: Sucess,
    message,
    [
      {text: 'ok', style: 'cancel'},
    ],
    {cancelable: false},
  );
}

export default Basecomponents;
