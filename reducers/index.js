import {
  SIGNUP_USER_STARTED,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAILURE,

  LOGIN_STARTED,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,

  GET_HISTORY_STARTED,
  GET_HISTORY_SUCCESS,
  GET_HISTORY_FAILURE,

  GET_USER_REWARD_POINT_STARTED,
  GET_USER_REWARD_POINT_SUCCESS,
  GET_USER_REWARD_POINT_FAILURE,

  GET_CATEGORY_STARTED,
  GET_CATEGORY_SUCCESS,
  GET_CATEGORY_FAILURE,

  GET_SUBCATEGORY_STARTED,
  GET_SUBCATEGORY_SUCCESS,
  GET_SUBCATEGORY_FAILURE,

  GET_SEARCHBUSINESS_STARTED,
  GET_SEARCHBUSINESS_SUCCESS,
  GET_SEARCHBUSINESS_FAILURE
} from '../types';
import {createReducer} from '../util/reduxUtil';
import Immutable from 'seamless-immutable';



export const initialState = Immutable.from({
  loading: false,
  age: 20,
  getRewardsData:null,
rewardsPointsData:null,
categoryArray:null,
subCategoryArray:null,
searchBusinessArray:[],

});


export const ongetHistoryCallStarted = state =>
  state.merge({
    loading: true,

    
  });
export const ongetHistoryCallSuccess = (state, response) =>
  state.merge({
    loading: false,
    getRewardsData: response.data

})
;
export const ongetHistoryCallFailure = state =>
  state.merge({
    loading: false,

    getRewardsData: initialState.getRewardsData
  
  }); 


export const ongetRewardsPointsCallStarted = state =>
  state.merge({
    loading: true,

    
  });
export const ongetRewardsPointsCallSuccess = (state, response) =>
  state.merge({
    loading: false,
    rewardsPointsData: response.response.data.data

})
;
export const ongetRewardsPointsCallFailure = state =>
  state.merge({
    loading: false,

    rewardsPointsData: initialState.rewardsPointsData
  
  }); 

export const getCategoryCallStarted = state =>
  state.merge({
    loading: true,

    
  });
export const getCategoryCallSuccess = (state, response) =>
  state.merge({
    loading: false,
    categoryArray: response.response.data.data
})
;
export const getCategoryCallFailure = state =>
  state.merge({
    loading: false,
    categoryArray: initialState.categoryArray
  
  }); 


  export const getSubCategoryCallStarted = state =>
  state.merge({
    loading: true,  
  });
export const getSubCategoryCallSuccess = (state, response) =>
  state.merge({
    loading: false,
    subCategoryArray: response.response.data.data
})
;
export const getSubCategoryCallFailure = state =>
  state.merge({
    loading: false,
    subCategoryArray: initialState.subCategoryArray
  
  }); 

  export const getSearchBusinessCallStarted = state =>
  state.merge({
    loading: true,  
  });
export const getSearchBusinessCallSuccess = (state, response) =>
  state.merge({
    loading: false,
    searchBusinessArray: response.response.data.data
})
;
export const getSearchBusinessCallFailure = state =>
  state.merge({
    loading: false,
    searchBusinessArray: initialState.searchBusinessArray
  
  }); 

const appReducer = createReducer(initialState, {
 

  [GET_HISTORY_STARTED]: ongetHistoryCallStarted,
  [GET_HISTORY_SUCCESS]: ongetHistoryCallSuccess,
  [GET_HISTORY_FAILURE]: ongetHistoryCallFailure,


  [GET_USER_REWARD_POINT_STARTED]: ongetRewardsPointsCallStarted,
  [GET_USER_REWARD_POINT_SUCCESS]: ongetRewardsPointsCallSuccess,
  [GET_USER_REWARD_POINT_FAILURE]: ongetRewardsPointsCallFailure,

  [GET_CATEGORY_STARTED]: getCategoryCallStarted,
  [GET_CATEGORY_SUCCESS]: getCategoryCallSuccess,
  [GET_CATEGORY_FAILURE]: getCategoryCallFailure,

  [GET_SUBCATEGORY_STARTED]: getSubCategoryCallStarted,
  [GET_SUBCATEGORY_SUCCESS]: getSubCategoryCallSuccess,
  [GET_SUBCATEGORY_FAILURE]: getSubCategoryCallFailure,

  [GET_SEARCHBUSINESS_STARTED]: getSearchBusinessCallStarted,
  [GET_SEARCHBUSINESS_SUCCESS]: getSearchBusinessCallSuccess,
  [GET_SEARCHBUSINESS_FAILURE]: getSearchBusinessCallFailure,


});

export default appReducer;

