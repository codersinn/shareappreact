/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import React ,{Component} from 'react'
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import AppStackNavigation from './Component/AppStackNavigation'
import NewStack from './NewStack'
import Splash from './Component/Splash';
import Login from './Component/Login';
import LoginScreen from './Component/LoginScreen';
import Authloading from './Component/AuthLoadingScreen';

import { Provider } from 'react-redux';
import { store } from './Store';
class Main extends Component{
    constructor(props){
        super(props);
        this.state = {currentScreen: 'Splash'};
        setTimeout(()=>{
            this.setState({currentScreen:'App'})
        },4000)
    }
    render(){
       
        const{currentScreen} =this.state
        let mainScreen = currentScreen==='Splash' ?  <Splash/>: <NewStack/>
        return  <Provider store={store}>
       {mainScreen}
      </Provider>
    }
}

AppRegistry.registerComponent(appName, () => Main
);


