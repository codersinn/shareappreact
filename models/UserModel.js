import * as Constants from '../Constant'
import {callApi} from '../models/BaseModel'

export default class UserModel {
    static signUp(requestBody){        
       return callApi(Constants.METHOD_TYPE_POST,Constants.API_SIGNUP,requestBody)
    }
    static login(requestBody){        
       return callApi(Constants.METHOD_TYPE_POST,Constants.API_LOGIN,requestBody)
    }

}