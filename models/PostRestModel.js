import * as Constants from '../Constant'
import {callApi} from './BaseModel'

export default class GetRestModel {
    static getHistory(requestBody){        
        return callApi(Constants.METHOD_TYPE_POST,Constants.API_GET_HISTORY,requestBody)
     }
    static getUserRewardsPoints(requestBody){        
        return callApi(Constants.METHOD_TYPE_POST,Constants.API_GET_REWARDS_POINTS,requestBody)
     }
    static getCategory(requestBody){        
        return callApi(Constants.METHOD_TYPE_POST,Constants.API_GET_CATEGORY)
     }
    static getSubCategory(requestBody){        
        return callApi(Constants.METHOD_TYPE_POST,Constants.API_GET_SUB_CATEGORY,requestBody)
     }
    static getSearchBusiness(requestBody){        
        return callApi(Constants.METHOD_TYPE_POST,Constants.API_SEARCH_BUSINESS,requestBody)
     }
    

}