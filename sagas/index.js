import { put, takeLatest, all } from 'redux-saga/effects';
import {
    SIGNUP_REQUEST,
    LOGIN_REQUEST,

    LOOKUP_REQUEST,
    GET_HISTORY_REQUEST,

    GET_USER_REWARD_POINT_REQUEST,
    GET_CATEGORY_REQUEST,
    GET_SUBCATEGORY_REQUEST,
    GET_SEARCHBUSINESS_REQUEST
} from '../types';
import UserModel from '../models/UserModel'
import GetRestModel from '../models/GetRestModel'
import PostRestModel from '../models/PostRestModel'

import { showMessage } from '../common/BaseComponent'

import {
    signUpStarted,
    signUpSuccess,
    signUpFailure,

    loginStarted,
    loginSuccess,
    loginFailure,

    getHistoryStarted,
    getHistorySuccess,
    getHistoryFailure,


    getUserRewardsPointsStarted,
     getUserRewardsPointsSuccess,
     getUserRewardsPointsFailure,


    getCategoryStarted,
     getCategorySuccess,
     getCategoryFailure,


    getSubCategoryStarted,
     getSubCategorySuccess,
     getSubCategoryFailure,


    getSearchBusinessStarted,
     getSearchBusinessSuccess,
     getSearchBusinessFailure,
} from '../actions';



export function* signUpRequest(action) {
    try {
        yield put(signUpStarted());
        const responseObj = yield UserModel.signUp(action.requestBody);

        if (responseObj.statusCode === 200 || responseObj.statusCode === 201) {
            yield put(signUpSuccess(responseObj.data))
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            }
        } else {
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            } else {
                showMessage(true, strings('somethingWentWrong'))
            }
            yield put(signUpFailure())
        }
    } catch (error) {
        yield put(signUpFailure());
        showMessage(true, error)
    }
}

export function* loginRequest(action) {
    try {
        yield put(loginStarted());
        const responseObj = yield UserModel.login(action.requestBody);

        if (responseObj.statusCode === 200 || responseObj.statusCode === 201) {
            yield put(loginSuccess(responseObj.data))
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            }
        } else {
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            } else {
                showMessage(true, strings('somethingWentWrong'))
            }
            yield put(loginFailure())
        }
    } catch (error) {
        yield put(loginFailure());
        showMessage(true, error)

    }

}

export function*  getHistoryRequest(action) {
    try {
        yield put( getHistoryStarted());
        const responseObj = yield PostRestModel.getHistory(action.requestBody);
        if (responseObj.statusCode === 200 || responseObj.statusCode === 201) {
            yield put( getHistorySuccess(responseObj))
            
        } else {
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            } else {
                showMessage(true, strings('somethingWentWrong'))
            }
            yield put( getHistoryFailure())
        }
    } catch (error) {
        yield put( getHistoryFailure());
        showMessage(true, error)

    }

}


export function*  getUserRewardsPointsRequest(action) {
    try {
        yield put( getUserRewardsPointsStarted());
        const responseObj = yield PostRestModel.getUserRewardsPoints(action.requestBody);
        if (responseObj.statusCode === 200 || responseObj.statusCode === 201) {
            yield put( getUserRewardsPointsSuccess(responseObj))
            
        } else {
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            } else {
                showMessage(true, strings('somethingWentWrong'))
            }
            yield put( getUserRewardsPointsFailure())
        }
    } catch (error) {
        yield put( getUserRewardsPointsFailure());
        showMessage(true, error)

    }

}



export function*  getAllCategoryRequest(action) {
    try {
        yield put( getCategoryStarted());
        const responseObj = yield PostRestModel.getCategory(action.requestBody);
        if (responseObj.statusCode === 200 || responseObj.statusCode === 201) {
            yield put( getCategorySuccess(responseObj))
            
        } else {
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            } else {
                showMessage(true, strings('somethingWentWrong'))
            }
            yield put( getCategoryFailure())
        }
    } catch (error) {
        yield put( getCategoryFailure());
        showMessage(true, error)

    }

}

export function*  getAllSubCategoryRequest(action) {
    try {
        yield put( getSubCategoryStarted());
        const responseObj = yield PostRestModel.getSubCategory(action.requestBody);
        if (responseObj.statusCode === 200 || responseObj.statusCode === 201) {
            yield put( getSubCategorySuccess(responseObj))
            
        } else {
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            } else {
                showMessage(true, strings('somethingWentWrong'))
            }
            yield put( getSubCategoryFailure())
        }
    } catch (error) {
        yield put( getSubCategoryFailure());
        showMessage(true, error)

    }

}

export function*  getSearchBusinessRequest(action) {
    try {
         yield put( getSearchBusinessStarted());
        const responseObj = yield PostRestModel.getSearchBusiness(action.requestBody);
        if (responseObj.statusCode === 200 || responseObj.statusCode === 201) {
            yield put( getSearchBusinessSuccess(responseObj))
            
        } else {
            if (responseObj.data && (responseObj.data.message)) {
                showMessage(true, responseObj.data.message)
            } else {
                showMessage(true, strings('somethingWentWrong'))
            }
            yield put( getSearchBusinessFailure())
        }
    } catch (error) {
        yield put( getSearchBusinessFailure());
        showMessage(true, error)

    }

}


export function* actionWatcher() {
    yield takeLatest(SIGNUP_REQUEST, signUpRequest)
    yield takeLatest(LOGIN_REQUEST, loginRequest)
    yield takeLatest(GET_HISTORY_REQUEST,  getHistoryRequest)
    yield takeLatest(GET_USER_REWARD_POINT_REQUEST,  getUserRewardsPointsRequest)
    yield takeLatest(GET_CATEGORY_REQUEST,  getAllCategoryRequest)
    yield takeLatest(GET_SUBCATEGORY_REQUEST,  getAllSubCategoryRequest)
    yield takeLatest(GET_SEARCHBUSINESS_REQUEST,  getSearchBusinessRequest)
}
export default function* rootSaga() {
    yield all([
        actionWatcher(),
    ]);
}
